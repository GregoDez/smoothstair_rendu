/************************************************************************
 * Author : Mendez Grégory
 * Fichier : tools.js
 * Description : Fichier contenant des fontioncs utiles
 ************************************************************************/
export function capitalizeFirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
};

export function changeActiveTab(id) {
    let elements = [document.getElementById("home"), document.getElementById("placeTab"), document.getElementById("profil"), document.getElementById("us")];
    switch (id) {
      case 0:
        elements[0]?.classList.remove("active-icon");
        elements[1]?.classList.remove("active-icon");
        elements[2]?.classList.remove("active-icon");
        elements[3]?.classList.remove("active-icon");
        break;
      case 1:
        elements[0]?.classList.add("active-icon");
        elements[1]?.classList.remove("active-icon");
        elements[2]?.classList.remove("active-icon");
        elements[3]?.classList.remove("active-icon");
        break;
      case 2:
        elements[0]?.classList.remove("active-icon");
        elements[1]?.classList.add("active-icon");
        
        elements[2]?.classList.remove("active-icon");
        elements[3]?.classList.remove("active-icon");
        break;
      case 3:
        elements[2]?.classList.add("active-icon");
        elements[0]?.classList.remove("active-icon");
        elements[1]?.classList.remove("active-icon");
        elements[3]?.classList.remove("active-icon");
        break;
      case 4:
        elements[3]?.classList.add("active-icon");
        elements[0]?.classList.remove("active-icon");
        elements[1]?.classList.remove("active-icon");
        elements[2]?.classList.remove("active-icon");
        break;
      default:
        break;
    }
  }