/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : RadiosGroup.tsx
 * Description : Container pour la génération des réponses à chaque question.
 ************************************************************************/

import { IonRadioGroup } from "@ionic/react";
import Radio from "./Radio";
import "./RadiosGroup.css";

type Props = {
  answers: Array<String> | Array<Array<String>>;
  //flexDir?: String;
  setIsAssociatedRadioChecked: Function;
  idQuestion: number;
  arrayRadiosValidation: Boolean[];
  setArrayRadiosValidation: Function;
  setFormResponsesTmp: Function;
  formResponsesTmp: any;
};

const RadiosGroup = (props: Props) => {
  const { answers, setIsAssociatedRadioChecked, idQuestion, setArrayRadiosValidation, arrayRadiosValidation, setFormResponsesTmp, formResponsesTmp } = props;

  
  return (
    <IonRadioGroup className="primaryFont" value="test">
      <div className="container__radios">
        {answers.map((answer: any, index: number) => {
          return (
            <Radio
              key={`radio-${index}`}
              idQuestion={idQuestion}
              idAnswer={answer.id}
              value={`${index}-${answer.answer}`}
              label={answer.answer}
              setIsAssociatedRadioChecked={setIsAssociatedRadioChecked}
              setFormResponses={setFormResponsesTmp}
              formResponsesTmp={formResponsesTmp}
            />
          );
        })}
      </div>
    </IonRadioGroup>
  );
};
export default RadiosGroup;
