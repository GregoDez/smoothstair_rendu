/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : Radio.tsx
 * Description : input de type radio avec un label associé.
 ************************************************************************/

import { IonItem, IonLabel, IonRadio } from "@ionic/react";
import "./Radio.css";
import { useEffect, useRef, useState } from "react";

const Radio = (props: any) => {
  const ref = useRef<HTMLIonRadioElement>(null);
  const [checkChange, setCheckChange] = useState(false);
  const onFocusEvent = async () => {
    await props.setIsAssociatedRadioChecked(true);
  };


  const handleKeyDown = async (event: { key: string }) => {
    if (event.key === "Enter") {
      // 👇️ your logic here
      handleRadioValue();
      await props.setIsAssociatedRadioChecked(true);
    }
  };

  const handleRadioValue = async () => {
    const element = ref.current;
    
    if(element !== null ){
      props.formResponsesTmp.push([props.idQuestion, props.idAnswer]);
    }
  }

  return (
    <IonItem className="primaryFont" tabIndex={0} lines="none">
      <IonLabel>{props.label}</IonLabel>
      <IonRadio tabIndex={0} onKeyDown={handleKeyDown} ref={ref} onClick={handleRadioValue} mode="md" slot="start" value={props.label} />
    </IonItem>
  );
};
export default Radio;
