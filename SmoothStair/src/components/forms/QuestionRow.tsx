import { RadiosGroup } from "..";
import "./Question.css";
import { Question } from "../../typescript/types/Question";
import { useEffect, useState } from "react";

type QuestionRow = {
  question: any;
  noQuestion: number;
  arrayRadiosValidation: Boolean[];
  setArrayRadiosValidation: Function;
  setFormResponsesTmp: Function;
  formResponsesTmp: any;
};

const QuestionRow = ({question, noQuestion, arrayRadiosValidation, setArrayRadiosValidation, setFormResponsesTmp, formResponsesTmp} : QuestionRow) => {
  let test: any[] = [];
  const [answers, setAnswers] = useState([]);
  const [filteredAnswers, setFilteredAnswers] = useState(test);
  const [questionHasAnswers, setQuestionHasAnswers] = useState([]);
  const [isAssociatedRadioChecked, setIsAssociatedRadioChecked] = useState(false);

  
  useEffect(() => {
    fetch('https://api.smoothstair.com/questionHasAnswers/' + question.id)
      .then((response) => response.json())
      .then((data) => {
          
        setQuestionHasAnswers(data);
        
      })
      .catch((err) => {
        console.log(err.message);
    });
    
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/answers')
       .then((response) => response.json())
       .then((data) => {
          
        setAnswers(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    test = [];

    questionHasAnswers.map((qa: any, j: number) => {
      answers.map((a: any, j: number) => {
        if(qa.idAnswer === a.id) {
          test.push(a);
        }
      })
    })

    
    setFilteredAnswers(test);
  }, [answers, questionHasAnswers]); 


  const updateArray = async () => {
    const newArray = arrayRadiosValidation.map((status, index) => (index === noQuestion ? (status = true) : status));

    setArrayRadiosValidation(newArray);
  };

  useEffect(() => {
    if (isAssociatedRadioChecked === true) {
      updateArray();
    }

    
  }, [isAssociatedRadioChecked]);

  return (
    <div className="Question__container primaryFont">
      <p tabIndex={0} aria-label={question.title + ". " + filteredAnswers.length + " réponses possibles : Oui ou Non"}>
        <span className="Question__indexnumber">{`${++noQuestion}.`}</span> {question.title}
      </p>
      {
        <RadiosGroup idQuestion={question.id} answers={filteredAnswers} formResponsesTmp={formResponsesTmp} arrayRadiosValidation={arrayRadiosValidation} setIsAssociatedRadioChecked={setIsAssociatedRadioChecked} setFormResponsesTmp={setFormResponsesTmp} setArrayRadiosValidation={setArrayRadiosValidation} />
      }
    </div>
  );
};
export default QuestionRow;
