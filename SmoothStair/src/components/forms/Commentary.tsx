/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : Button.tsx
 * Description : Composant bouton
 ************************************************************************/

import { IonIcon } from "@ionic/react";
import { heart, cancel, edit } from "../../icons";
import "./Commentary.css";
import { useEffect, useState } from "react";

interface Commentary {
  id: number;
  pseudo: string;
  commentary: string;
  nbLikes: number;
  date: string;
  idFormReponse: number;
  idUser: number;
  deletedCommentary: Function;
  setCommentary: Function;
  setIdCommentary: Function;
  commentaryInputRef: any;
}

const Commentary: React.FC<Commentary> = ({ id, pseudo, commentary, nbLikes, date, idFormReponse, idUser, setCommentary, setIdCommentary, deletedCommentary, commentaryInputRef }) => {
  const [nbLike, setNbLike] = useState(0);
  const [userHasLiked, setUserHasLiked] = useState(false);
  const [likesArray, setLikesArray] = useState([]);
  const [commentaries, setCommentaries] = useState([]);
  const [idLike, setIdLike] = useState(0);
  
  const [modifiedCommentary, setModifiedCommentary] = useState(false);
  
  let today = new Date();
  let commentaryDate = new Date(date);

  let difference = today.getTime() - commentaryDate.getTime();
  difference = difference / (1000 * 3600 * 24);

  let cancelOpen = false;
  let isLogged = false;
  let style = "";

  if(localStorage.getItem("idUser") === idUser.toString() || localStorage.getItem("idRole") === "1" || localStorage.getItem("idRole") === "2") {
    cancelOpen = true;
  } else {
    cancelOpen = false;
  }

  if(localStorage.getItem("isLogged") === "true") {
    isLogged = true;
  } else {
    isLogged = false;
    cancelOpen = false;
  }

  useEffect(() => {
    fetch('https://api.smoothstair.com/commentaries')
       .then((response) => response.json())
       .then((data) => {

        for (let i = 0; i < data.length; i++) {
          const element = data[i];
          if(element.id === id) {
            setNbLike(element.nbLikes)
          }
        }
        setCommentaries(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    if(likesArray.length !== 0) {
      likesArray.map((l: any, i: number) => {
        if (l.idCommentary === id && l.idUser.toString() === localStorage.getItem("idUser")) {
          setUserHasLiked(true);
          setIdLike(l.id)
        } else {
          setUserHasLiked(false);
        }
      });
    } else {
      setUserHasLiked(false);
    }
  }, [likesArray])

  useEffect(() => {
    fetch('https://api.smoothstair.com/userLikesCommentaries/byIdCommentary/' + id, {
      method: 'get',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
    })
    .then((response) => response.json())
    .then((data) => {
      if(data === "No likes found") {
        setLikesArray([]);
      } else {
        setLikesArray(data);
      }
    })
    .catch((err) => {
      console.log(err.message);
    });
  }, [modifiedCommentary]); 

  const editCommentary = () => {
    setCommentary(commentary)
    setIdCommentary(id)
    commentaryInputRef.current?.focus()
  }

  const deleteCommentary = () => {
    deletedCommentary(false);
    fetch('https://api.smoothstair.com/commentaries/' + id, {
      method: 'delete',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
    })
    .then((response) => response.json())
    .then((data) => {
      deletedCommentary(true);
    })
    .catch((err) => {
      console.log(err.message);
    });
  }

  let commentaryTime = "";

  let nbDifferenceDay = Math.round(difference) -1;

  if(Math.round(difference) -1 === 0) {
    commentaryTime = commentaryDate.toLocaleDateString();
  } else {
    commentaryTime = commentaryDate.toLocaleDateString();
    
  }

  let updateLike =  () => {
    setModifiedCommentary(false);
    if(userHasLiked) {
      fetch('https://api.smoothstair.com/userLikesCommentaries/' + idLike, {
        method: 'delete',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
      })
      .then((response) => response.json())
      .then((data) => {        
        if(data === "Deleted a like successfully") {
          fetch('https://api.smoothstair.com/commentaries/' + id, {
            method: 'put',
            headers: { 
              'Content-Type': 'application/json',
              'Accept' : 'application/json' 
            },
            body: JSON.stringify({
              nbLikes: nbLike-1,
              commentary: commentary
            }),
          })
          .then((response) => response.json())
          .then((data) => {
            setModifiedCommentary(true);
            setNbLike(nbLike-1)
          })
          .catch((err) => {
            console.log(err.message);
          });
        }
        
      })
      .catch((err) => {
        console.log(err.message);
      });
    } else {
      fetch('https://api.smoothstair.com/userLikesCommentaries', {
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify({
          idUser: localStorage.getItem("idUser"),
          idCommentary: id,
        }),
      })
      .then((response) => response.json())
      .then((data) => {
        setModifiedCommentary(true);
        fetch('https://api.smoothstair.com/commentaries/' + id, {
          method: 'put',
          headers: { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json' 
          },
          body: JSON.stringify({
            nbLikes: nbLike+1,
            commentary: commentary
          }),
        })
        .then((response) => response.json())
        .then((data) => {
          setModifiedCommentary(true);
          setNbLike(nbLike+1)
        })
        .catch((err) => {
          console.log(err.message);
        });
        
      })
      .catch((err) => {
        console.log(err.message);
      });
    }
  }

  if (userHasLiked) {
    style = "icon heartIcon heartFillIcon";
  } else {
    style = "icon heartIcon";
  }

  const handleLikeKeyDown = (event: { key: string; }) => {
    console.log('User pressed: ', event.key);

    // console.log(message);

    if (event.key === 'Enter') {
      // 👇️ your logic here
      updateLike();
    }
  }

  const handleEditCommentaryKeyDown = (event: { key: string; }) => {
    console.log('User pressed: ', event.key);

    // console.log(message);

    if (event.key === 'Enter') {
      // 👇️ your logic here
      editCommentary();
    }
  }

  const handleDeleteCommentaryKeyDown = (event: { key: string; }) => {
    console.log('User pressed: ', event.key);

    // console.log(message);

    if (event.key === 'Enter') {
      // 👇️ your logic here
      deleteCommentary();
    }
  }

  return (
    <div className="commentary">
      <img className="userImg" src={"../assets/images/places/kraken.jpg"} alt="" />
      <div className="commentaryContent">
        <div className="commentaryText" tabIndex={0}><b>{pseudo}</b> {commentary}</div>
        <div className="commentaryInfo">
          <div className="commentaryTime" tabIndex={0}>{commentaryTime}</div>
          <div className="commentaryLikes" tabIndex={0}>{nbLike} likes</div> 
        </div>
      </div>
      <div className="commentaryIcons">
        <div className="commentaryLike">
          {localStorage.getItem("isLogged") === "true" && idUser.toString() !== localStorage.getItem("idUser") ? <div tabIndex={0}  className="commentaryLikeIcon" aria-label="Aimer ou ne plus aimer le commentaire" onKeyDown={handleLikeKeyDown} onClick={updateLike}><IonIcon className={style} src={heart}  /></div> : null}
        </div>
        
        <div className="commentaryActions">
          { localStorage.getItem("isLogged") === "true" && idUser.toString() === localStorage.getItem("idUser") ? <div tabIndex={0} onKeyDown={handleEditCommentaryKeyDown} aria-label="Modifier votre commentaire" className="commentaryEditIcon" onClick={editCommentary}><IonIcon  name="close-circle-outline" className="icon" src={edit}/></div> : null}
          { localStorage.getItem("isLogged") === "true" && cancelOpen ? <div className="commentaryLikeIcon" tabIndex={0} onKeyDown={handleDeleteCommentaryKeyDown} aria-label="Supprimer votre commentaire" onClick={deleteCommentary}><IonIcon  name="close-circle-outline" className="icon" src={cancel}/></div> : null}
        </div>
      </div>
      
    </div>
  );
};

export default Commentary;


