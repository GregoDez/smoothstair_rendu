import { QuestionRow, Button, InputArea } from "../";

import { SetStateAction, useEffect, useState } from "react";
import { IonLabel, useIonRouter } from "@ionic/react";
import { useHistory, useLocation } from "react-router-dom";

const FormQuestionnary = () => {
  const router = useIonRouter();
  const history = useHistory();
  const location = useLocation();
  const [forms, setForms] = useState([]);
  const [filledForms, setFilledForms] = useState([]);
  const [formHasQuestions, setFormHasQuestions] = useState([]);
  const [disabilities, setDisabilities] = useState([]);
  const [questions, setQuestions] = useState([]);
  const [formResponsesTmp, setFormResponsesTmp] = useState([]);
  const [formResponses, setFormResponses] = useState<any>([]);
  const [formDescription, setFormDescription] = useState("");
  const [place, setPlace]:any = useState([]); 
  const [errMessage, setErrMessage] = useState("");
  const [placeAccessibility, setPlaceAccessibility] = useState([]);

  const [idForm, setIdForm] = useState(0);
  const [created, setCreated] = useState(false);
  const [handleNotation, setHandleNotation] = useState(false);

  let currentPath = router.routeInfo.pathname;
  let path = currentPath.split("/");

  const idSubCategory = path[3];
  const idDisability = path[4];

  const placeData: any = location.state;
  
  let arrayQuestions: Boolean[] = [];
  let counterQuestions = 0;
  let nbQuestion = 0;
  let tmpArray: any[] = [];

  useEffect(() => {
    fetch('https://api.smoothstair.com/forms')
       .then((response) => response.json())
       .then((data) => {
          
        setForms(data);
        
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/disabilities')
       .then((response) => response.json())
       .then((data) => {
          
        setDisabilities(data);
        
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  /*useEffect(() => {
    fetch('https://api.smoothstair.com/filledForms/byIdPlace/' + place.id)
       .then((response) => response.json())
       .then((data) => {
          
        setFilledForms(data);
        
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, [place]);*/

  useEffect(() => {
    
    if(forms.length !== 0) {
      forms.map((f: any, i: number) => {
        if(f.idSubCategory === parseInt(idSubCategory) && f.idDisability === parseInt(idDisability)) {
          setIdForm(f.id)
        }
      })
    }
  }, [forms]);

  useEffect(() => {
    if(idForm !== 0) {
      fetch('https://api.smoothstair.com/formHasQuestions/' + idForm)
       .then((response) => response.json())
       .then((data) => {
          
        setFormHasQuestions(data);
        
       })
       .catch((err) => {
          console.log(err.message);
       });
    }
  }, [idForm]);

  

  useEffect(() => {
    fetch('https://api.smoothstair.com/questions')
       .then((response) => response.json())
       .then((data) => {
          
        setQuestions(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    if(!created) {
      if(place === null || place.id !== undefined) {
        if(place !== "No place found" && place !== null) { // if exist
          createFilledForm(place.id, true)
          checkIfPlaceAccesibilityExist()
        } else { // if not exist
          createPlace() // create place & get id
          setCreated(true)
        }
        
      }
    }
    
    
  }, [place]);

  useEffect(() => {
    if(placeAccessibility !== null) {
      placeAccessibility.map((pa: any, i: number) => {
        
        if(pa.idDisability === parseInt(idDisability)) {
          if(pa.idNotation === 6) {
            updatePlaceAccessibility(pa.id)
          } 
        }
      })
    }
      
      
  }, [placeAccessibility]);



  

  const handleFormDescription = (event: { target: { value: SetStateAction<string>; }; }) => {
    setFormDescription(event.target.value); 
  }

  const handleSubmit = () => {
    let finalFormResponses: any[] = [];
    let idPlace = null;

    // Garde uniquement les dernières réponses aux questions
    formResponsesTmp.map((f: any, i: number) => {
      formHasQuestions.map((fq: any, j: number) => {
        if(f[0] === fq.idQuestion) {
          finalFormResponses[j] = [f[0], f[1]];
        }
      })
    })

    if(formHasQuestions.length === finalFormResponses.length) {
      // Vérifie si l'établissement existe dans la base de données
      checkIfPlaceExist(placeData.name, placeData.address)
      setFormResponses(finalFormResponses);
    } else {
      setErrMessage("Veuillez répondre à toutes les questions")
    }
  }
  
  const checkIfPlaceExist = (name: string, address: string) => {
    fetch('https://api.smoothstair.com/places/' + name + "/" + address, {
      method: 'get',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
    })
    .then((response) => response.json())
    .then((data) => {
      let result
      if(data !== "No place found") {
        result = data
      } else {
        result = null
      }
      
      setPlace(result)
    })
    .catch((err) => {
      console.log(err.message);
    })
  }

  const createPlace = () => {
    let payload = {
      name: placeData.name,
      address: placeData.address,
      latitude: placeData.latitude,
      longitude: placeData.longitude,
      idSubCategory: idSubCategory,
    };

    fetch('https://api.smoothstair.com/places', {
      method: 'post',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
      body: JSON.stringify(payload),
    })
    .then((response) => response.json())
    .then((data) => {
      createFilledForm(data.id, false)
      setPlace(data)
    })
    .catch((err) => {
      console.log(err.message);
    });
  }

  const createPlaceAccessibility = (id: any, notation: any) => {
    disabilities.map((d: any, i: number) => {
      let idNotation = d.id === parseInt(idDisability) ? notation : 6
      let payload = {
        idPlace: id,
        idDisability: d.id,
        idNotation: idNotation,
      };

      fetch('https://api.smoothstair.com/placesAccessibilities', {
      method: 'post',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
      body: JSON.stringify(payload),
      })
      .then((response) => response.json())
      .then((data) => {
        let result
        if(data !== "No accessibility for this place found") {
          result = data
        } else {
          result = null
        }
        
      })
      .catch((err) => {
        console.log(err.message);
      })
    })

    
  }

  const createFilledForm = (id: any, exist: boolean) => {
    let today = new Date()
    let date;
    let month = (today.getMonth() + 1)
    let monthResult;
    if(month < 10) {
      monthResult = "0" + month.toString()
    }

    date = today.getDate() + '/' + (monthResult) + '/' + today.getFullYear()
    
    let notation = 1
    formResponses.map((fr: any, i: number) => {
      questions.map((q: any, j: number) => {
        if(fr[0] === q.id) {
          if(fr[1] === 2) {
            notation = notation + q.ponderation

            if(notation > 5)
            {
              notation = 5
            }
          }
        }
      })
    })

    
    let payload = {
      idForm: idForm,
      idPlace: id,
      idUser: localStorage.getItem("idUser"),
      description: formDescription,
      idNotation: notation,
      nbLike: 0,
      date: date,
    };
  
    fetch('https://api.smoothstair.com/filledForms', {
      method: 'post',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
      body: JSON.stringify(payload),
    })
    .then((response) => response.json())
    .then((data) => {
      createFilledFormResponse(data.id)

      if(!exist){
        createPlaceAccessibility(data.idPlace, notation)
      }
      
    })
    .catch((err) => {
      console.log(err.message);
    });
  }

  const createFilledFormResponse = (id: any) => {
    formResponses.map((fr: any, i: number) => {
      
      let payload = {
        idFilledForm: id,
        idQuestion: fr[0],
        idAnswer: fr[1],
      };
      
      fetch('https://api.smoothstair.com/filledFormResponses', {
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify(payload),
      })
      .then((response) => response.json())
      .then((data) => {

        history.push("/home")
      })
      .catch((err) => {
        console.log(err.message);
      });
    })
  }

  const checkIfPlaceAccesibilityExist = () => {
    if(place.id !== undefined && place.id !== null) {
      fetch('https://api.smoothstair.com/placesAccessibilities/byPlaceId/' + place.id, {
      method: 'get',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
    })
    .then((response) => response.json())
    .then((data) => {
      let result
      if(data !== "No accessibility for this place found") {
        result = data
      } else {
        result = null
      }
      
      setPlaceAccessibility(result)
    })
    .catch((err) => {
      console.log(err.message);
    })
    }
    
  }

  const updatePlaceAccessibility = (id: any) => {
    let notation = 1
      formResponses.map((fr: any, i: number) => {
        questions.map((q: any, j: number) => {
          if(fr[0] === q.id) {
            if(fr[1] === 2) {
              notation = notation + q.ponderation
  
              if(notation > 5)
              {
                notation = 5
              }
  
              
            }
          }
        })
      })
    

    let payload = {
      idDisability: idDisability,
      idPlace: place.id,
      idNotation: notation,
    };

    fetch('https://api.smoothstair.com/placesAccessibilities/' + id, {
      method: 'put',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
      body: JSON.stringify(payload),
    })
    .then((response) => response.json())
    .then((data) => {
      
    })
    .catch((err) => {
      console.log(err.message);
    });
  }
  
  formHasQuestions.map((f: any, i: number) => {
    nbQuestion++
  })

  let [arrayRadiosValidation, setArrayRadiosValidation] = useState(arrayQuestions);
  
  return (
    <div className="primaryFont">
      <div className="numbersOfQuestions" tabIndex={0}>
        {nbQuestion} Questions au total !
      </div>
      <p tabIndex={0}>Souhaitez vous ajouter un <IonLabel color="secondary">commentaire</IonLabel>  ?</p>
      <textarea name="textArea" className="textArea" aria-label="Entrez une description personnalisée (facultatif)" onChange={handleFormDescription} placeholder="Entrez votre commentaire"></textarea>
      {
        formHasQuestions.map((fq: any, i: number) => {
          if(fq.idForm === idForm) {
            return questions.map((q: any, j: number) => {
              if(q.id === fq.idQuestion )
              return <QuestionRow
              question={q}
              noQuestion={counterQuestions++}
              key={`quest-${i}-${q.id}-${fq.idDisability}-${fq.idSubCategory}`}
              arrayRadiosValidation={arrayRadiosValidation}
                      setArrayRadiosValidation={setArrayRadiosValidation}
                      setFormResponsesTmp={setFormResponsesTmp}
                      formResponsesTmp={formResponsesTmp}
            />
            })
          }
        })}
        {errMessage !== "" && <div className="errorMessage">{errMessage}</div>}
        <button onClick={handleSubmit} className="buttonContainer commonButton">Soumettre</button>
    </div>
  );
};
export default FormQuestionnary;
