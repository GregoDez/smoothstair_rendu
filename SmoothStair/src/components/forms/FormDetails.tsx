/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : PlaceCard.tsx
 * Description : Composant carte établissement
 ************************************************************************/

import { IonIcon, useIonRouter, IonCard } from "@ionic/react";

import "./FormDetails.css";
import "../../theme/icons.css";

/* Custom assets */

import { banner, cancel, dislike, like } from "../../icons";

import { Input, Button, Commentary } from "..";

import { SetStateAction, useEffect, useRef, useState } from "react";

import {
  wheelChair as wheelChair,
  blind as blind,
  awesome,
  good,
  satisfaisant,
  bof,
  angry,
  nonRenseigner,
} from "../../icons/";
import { useHistory } from "react-router";

type Form = {
  id: number;
  idForm: number;
  idPlace: number;
  answers: Array<String>;
  fontFamily: any;
};

const FormDetails = ({ id, idPlace, fontFamily }: Form) => {
  const history = useHistory()
  const [forms, setForms] = useState([]);
  const [filledForms, setFilledForms] = useState([]);
  const [filledFormResponses, setFilledFormResponses] = useState([]);
  const [commentaries, setCommentaries] = useState([]);
  const [notations, setNotations] = useState([]);
  const [disabilities, setDisabilities] = useState([]);
  const [commentary, setCommentary] = useState("");
  const [commentaryCreated, setCommentaryCreated] = useState(false);
  const [commentaryDeleted, setCommentaryDeleted] = useState(false);
  const [deletedFilledForm, setDeletedFilledForm] = useState(false);
  const [idCommentary, setIdCommentary] = useState(null);
  const [idDisability, setIdDisability] = useState(null);
  const [nbLike, setNbLike] = useState<any>(null);
  const [modifiedForm, setModifiedForm] = useState(false);
  const [userHasLiked, setUserHasLiked] = useState(false);
  const [likesArray, setLikesArray] = useState([]);
  const [placeAccess, setPlaceAccess] = useState([]);
  const [idLike, setIdLike] = useState(0);
  const [idNotation, setIdNotation] = useState<any>(null);
  const [idPlaceAccess, setIdPlaceAccess] = useState<any>(null);
  const [counterFilledForms, setCounterFilledForms] = useState<any>(null);
  
  
  const router = useIonRouter();
  const [hiddenCommentaries, setHiddenCommentaries] = useState(true);
  const [buttonViewCommentaryText, setButtonViewCommentaryText] = useState("");
  const commentaryInput = useRef<HTMLInputElement>(null);
  let currentPath = router.routeInfo.pathname;
  
  let path = currentPath.split("/");

  let isLogged = false;
  let href = ""
  let srcIcon: string | undefined;
  let nbCommentaries = 0;
  let notationIcon: any | undefined, notationTitle: string;

  let cancelOpen = false;

  if(localStorage.getItem("idRole") === "1" || localStorage.getItem("idRole") === "2") {
    cancelOpen = true;
  } else {
    cancelOpen = false;
  }

  if(localStorage.getItem("isLogged") === "true") {
    isLogged = true;
  } else {
    isLogged = false;
    cancelOpen = false;
  }


  if(localStorage.getItem("isLogged") === "true") {
    isLogged = true;
  } else {
    isLogged = false;
  }

  useEffect(() => {
    if (hiddenCommentaries) setButtonViewCommentaryText("voir les commentaires");
    else setButtonViewCommentaryText("cacher les commentaires");
  }, [hiddenCommentaries]);

  useEffect(() => {
    fetch('https://api.smoothstair.com/filledForms')
       .then((response) => response.json())
       .then((data) => {
        

        for (let i = 0; i < data.length; i++) {
          const element = data[i];
          if(element.idPlace === idPlace && element.id === id) {
            setNbLike(element.nbLike)
          }
        }

        setFilledForms(data);
       })
       .catch((err) => {
          console.log(err.message);
       });

       fetch('https://api.smoothstair.com/placesAccessibilities/byPlaceId/' + idPlace, {
          method: 'get',
          headers: { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json' 
          },
        })
        .then((response) => response.json())
        .then((data) => {
          setPlaceAccess(data)
        })
        .catch((err) => {
          console.log(err.message);
        });
  }, [deletedFilledForm]);


  useEffect(() => {
    let nbLikeTmp = 0;
    if(counterFilledForms !== null) {
      placeAccess.map((pa: any, i: number) => {
        
        if(pa.idDisability === parseInt(path[5]) && pa.idPlace === idPlace){
          setIdPlaceAccess(pa.id)
          if(counterFilledForms === 0) {
            
            setIdNotation(6)
          } else { 
            fetch('https://api.smoothstair.com/filledForms/byIdPlace/' + pa.idPlace)
            .then((response) => response.json())
            .then((data) => {
              forms.map((f: any, i: number) => {
                
                if(data !== "No filled forms for id found " && f.idDisability === parseInt(path[5])) {
                  data.map((ff: any, i: number) => {
                    
                    if(ff.idPlace === pa.idPlace && f.id == ff.idForm) {
                      
                      if(ff.nbLike >= nbLikeTmp) {
                        nbLikeTmp = ff.nbLike
                        setIdNotation(ff.idNotation)
                      } 
                    }
                  })
                }
                
              })
              
            })
            .catch((err) => {
                console.log(err.message);
            });
            
          }
        }
      })
    }
  }, [counterFilledForms])

  useEffect(() => {
    let idNotationTMP = idNotation

    if(idPlaceAccess !== null) {

      if(idNotation === null) {
        idNotationTMP = 6
      }
      fetch('https://api.smoothstair.com/placesAccessibilities/' + idPlaceAccess, {
        method: 'put',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify({
          idDisability: parseInt(path[5]),
          idPlace: idPlace,
          idNotation: idNotationTMP,
        }),
      })
      .then((response) => response.json())
      .then((data) => {
        setDeletedFilledForm(true)
        if(counterFilledForms === 0) {
          history.push("/home/" + path[2] + "/" + path[3] + "/" + path[4]);
        }
        
      })
      .catch((err) => {
        console.log(err.message);
      });    
    }
    
  }, [idNotation , idPlaceAccess])

  useEffect(() => {
    
      fetch('https://api.smoothstair.com/userLikesFilledForms/byIdFilledForm/' + id, {
        method: 'get',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
      })
      .then((response) => response.json())
      .then((data) => {
        if(data === "No likes found") {
          setLikesArray([]);
        } else {
          setLikesArray(data);
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
    
    
  }, [modifiedForm]);   

  useEffect(() => {
    if(likesArray.length !== 0) {
      
      likesArray.map((l: any, i: number) => {
        if (l.idFilledForm === id && l.idUser.toString() === localStorage.getItem("idUser")) {
          setUserHasLiked(true);
          setIdLike(l.id)
        } 
      });
    } else {
      setUserHasLiked(false);
    }
    
  }, [likesArray])

  useEffect(() => {
    fetch('https://api.smoothstair.com/forms')
       .then((response) => response.json())
       .then((data) => {
          
        setForms(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/disabilities')
       .then((response) => response.json())
       .then((data) => {
          
        setDisabilities(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/commentaries')
       .then((response) => response.json())
       .then((data) => {
          
        setCommentaries(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
       
  }, [commentaryCreated, commentaryDeleted]);

  useEffect(() => {
    fetch('https://api.smoothstair.com/notations')
       .then((response) => response.json())
       .then((data) => {
          
        setNotations(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/filledFormResponses/byIdFilledForm/' + id)
       .then((response) => response.json())
       .then((data) => {
        setFilledFormResponses(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  if (path[1] === "home") {
    href = "/home/" + path[2] + "/" + path[3] + "/";
  } else if (path[1] === "describe") {
    href = "/describe/";
  }

  commentaries.map((c: any, i: number) => {
    if (c.idFilledForm === id) {
      nbCommentaries++;
    }
  });
  
  
  filledForms.map((r: any, i: number) => {
    if(r.idPlace === idPlace && r.id === id) {
      notations.map((n: any, i: number) => {
        if(r.idNotation === n.id) {
          switch (r.idNotation) {
            case 1:
              notationIcon = awesome;
              break;
            case 2:
              notationIcon = good;
              break;
            case 3:
              notationIcon = satisfaisant;
              break;
            case 4:
              notationIcon = bof;
              break;
            case 5:
              notationIcon = angry;
              break;
            case 6:
              notationIcon = nonRenseigner;
              break;
            default:
              notationIcon = nonRenseigner;
              break;
          }

          notationTitle = n.name;
        }
        
          
      })
    }
  })
  
  const changePath = () => {
    //history.push(href);
  };

  let handleCommentaries = () => {
    if (hiddenCommentaries) setHiddenCommentaries(false);
    else setHiddenCommentaries(true);
  };

  let today = new Date(),
  date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

  


  let handleInputChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setCommentary(event.target.value); 
  }

  let createCommentary = () => {
    
    if(idCommentary === null) {
      let payload = {
        idUser: localStorage.getItem("idUser"),
        idFilledForm: id,
        nbLikes: 0,
        date: date,
        commentaryText: commentary,
      };

      setCommentaryCreated(false);
      fetch('https://api.smoothstair.com/commentaries', {
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify(payload),
      })
      .then((response) => {response.json()})
      .then((data) => {
        setCommentaryCreated(true);
        setCommentary("");
      })
      .catch((err) => {
        console.log(err.message);
      });
    } else {
      let payload = {
        commentary: commentary,
        nbLikes: nbLike
      };
      setCommentaryCreated(false);
      fetch('https://api.smoothstair.com/commentaries/' + idCommentary, {
        method: 'put',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify(payload),
      })
      .then((response) => {response.json()})
      .then((data) => {
        setIdCommentary(null)
        setCommentaryCreated(true);
        setCommentary("");
      })
      .catch((err) => {
        console.log(err.message);
      });
    }
  }

  let updateLike =  () => {
    setModifiedForm(false);

    placeAccess.map((pa: any, i: number) => {
      if(pa.idPlace === idPlace && pa.idDisability === parseInt(path[5])) {
        setIdPlaceAccess(pa.idPlace)
      }
    })

    filledForms.map((ff: any, i: number) => {
      if(ff.id === id) {
        setIdNotation(ff.idNotation)
      }
    })

    if(userHasLiked) {
      fetch('https://api.smoothstair.com/userLikesFilledForms/' + idLike, {
        method: 'delete',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
      })
      .then((response) => response.json())
      .then((data) => {        
        if(data === "Deleted a like successfully") {
          fetch('https://api.smoothstair.com/filledForms/' + id, {
            method: 'put',
            headers: { 
              'Content-Type': 'application/json',
              'Accept' : 'application/json' 
            },
            body: JSON.stringify({
              nbLike: nbLike-1,
            }),
          })
          .then((response) => response.json())
          .then((data) => {
            setModifiedForm(true);
            setNbLike(nbLike-1);
            setUserHasLiked(false);
          })
          .catch((err) => {
            console.log(err.message);
          });
        }
        
      })
      .catch((err) => {
        console.log(err.message);
      });
    } else {
      fetch('https://api.smoothstair.com/userLikesFilledForms', {
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify({
          idUser: localStorage.getItem("idUser"),
          idFilledForm: id,
        }),
      })
      .then((response) => response.json())
      .then((data) => {
        setModifiedForm(true);
        fetch('https://api.smoothstair.com/filledForms/' + id, {
          method: 'put',
          headers: { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json' 
          },
          body: JSON.stringify({
            nbLike: nbLike+1
          }),
        })
        .then((response) => response.json())
        .then((data) => {
          setModifiedForm(true);
          setNbLike(nbLike+1)
        })
        .catch((err) => {
          console.log(err.message);
        });
        
      })
      .catch((err) => {
        console.log(err.message);
      });
    }
    commentaryInput.current?.focus()
  }

  const deleteFilledForm = () => {
    setDeletedFilledForm(false);
    fetch('https://api.smoothstair.com/filledForms/' + id, {
      method: 'delete',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
    })
    .then((response) => response.json())
    .then((data) => {
      setDeletedFilledForm(true);

      updatePlaceAccessibility()
    })
    .catch((err) => {
      console.log(err.message);
    });
  }

  const cancelCommentaryEdit = () => {
    setIdCommentary(null)
    setCommentaryCreated(false);
    setCommentary("");
  }

  const updatePlaceAccessibility = () => {
    let counterFilledFormsTmp = 0
    
    forms.map((f: any, i: number) => {
      //console.log(f.idDisability)
      filledForms.map((ff: any, i: number) => {
        if(ff.idPlace === idPlace && ff.idForm === f.id  && f.idSubCategory === parseInt(path[3]) && f.idDisability === parseInt(path[5])) {
          counterFilledFormsTmp++
        }
      }) 
    })

    setCounterFilledForms(counterFilledFormsTmp)    
  }

  const handleKeyDown = (event: { key: string; }) => {
    console.log('User pressed: ', event.key);

    // console.log(message);

    if (event.key === 'Enter') {
      // 👇️ your logic here
      updateLike();
    }
  };

  const handleCommentaryKeyDown = (event: { key: string; }) => {
    console.log('User pressed: ', event.key);

    // console.log(message);

    if (event.key === 'Enter') {
      // 👇️ your logic here
      createCommentary();
    }
  }

  const handleCancelCommentaryEditKeyDown = (event: { key: string; }) => {
    console.log('User pressed: ', event.key);

    // console.log(message);

    if (event.key === 'Enter') {
      // 👇️ your logic here
      cancelCommentaryEdit();
    }
  }

  

  return (
    <div className={"formCardContainer"}>
      { filledForms.length !== 0 ?
        forms.map((f: any, j: number) => {
          return filledForms.map((r: any, i: number) => {
            if (r.idPlace === idPlace && r.idForm === f.id && r.id === id && f.idDisability === parseInt(path[5]) ) {
              switch (r.idDisability) {
                case 1:
                  srcIcon = wheelChair;
                  break;
                case 2:
                  srcIcon = blind;
                  break;

                default:
                  srcIcon = wheelChair;
                  break;
              }
            
            return <>       
            <IonCard onClick={changePath} className={"formDetailsCardContainer" + fontFamily.fontFamily}>
            { localStorage.getItem("isLogged") === "true" && cancelOpen ? <div className="commentaryDeleteFormIconContainer" ><IonIcon onClick={deleteFilledForm} tabIndex={0} name="close-circle-outline" className="icon commentaryDeleteFormIcon" src={cancel}/></div> : null}
              <div className="formDetailsHeader">
                <div className="formNotation"><IonIcon tabIndex={0} aria-label={"notation : " + notationTitle} className="formNotationIcon" src={notationIcon} /></div>
                <div className="formUser">
                  <img className="userImg" src={"../assets/images/places/kraken.jpg"} alt="" />
                  <div className="formUserDetails">
                    <div tabIndex={0} className="formUserDetailsName">
                      {r.idPlace === idPlace && r.id === id ? r.pseudo : null }
                    </div>
                    <div tabIndex={0} className="formUserDetailsDate">Le {r.idPlace === idPlace && r.id === id ? r.date : null}
                    </div>
                  </div>
                </div>
                {
                  r.idPlace === idPlace && r.id === id && r.idDisability !== null ?
                  <div className="formUserStatus formUserStatusIcon"><IonIcon tabIndex={0} aria-label={"Mobilité de " + r.pseudo + " : " + r.disability} src={srcIcon} /></div>
                  : <div><div tabIndex={0} aria-label={"Role de " + r.pseudo + " : " + r.role} className="formUserRole">{r.role}</div></div>
                }
              </div>
              <div className="formDetails">
                <div tabIndex={0} aria-label={"Description de la réponse : " + r.description} className="formDetailsText">
                  {
                    r.idPlace === idPlace && r.id === id ? r.description : null
                  }
                </div>
                <div className="">
                  <div tabIndex={0} className="formDetailsAnswerTitle">Réponses au formulaire :</div>
                  <div className="formDetailsAnswersContainer">
                    {filledFormResponses.map((response: any, i: number) => {
                      return (
                        <div tabIndex={0} className="formDetailsAnswerContainer">
                          <div className="formDetailsQuestion">{response.question}</div>
                          <div className="formDetailsAnswer">{response.answer}</div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </IonCard>
            <div className={"formSocial" + fontFamily.fontFamily}>
                <div className="formLikeUnlike">
                  <div className="formLikeUnlikeContainer" onClick={updateLike} onKeyDown={handleKeyDown} tabIndex={0} aria-label={"Cliquez pour aimer ou ne plus aimer le formulaire. Nombre de j'aime : " + nbLike}>
                    {localStorage.getItem("isLogged") === "true" ? <div className="" ><IonIcon className="likeDislikeIcon" src={like} /></div> : null}
                    <div className="likeDislikeNumber">{nbLike} j'aime</div>
                  </div>
                  {nbCommentaries > 0 ?
                    <div className="viewButtonContainer"><button tabIndex={0} className="buttonContainer viewButton" onClick={handleCommentaries}>{buttonViewCommentaryText}</button></div>
                    :
                    <div className="viewButtonContainer"><button tabIndex={0} className="buttonContainer viewButton" onClick={handleCommentaries}>Pas de commentaires</button></div>}

                </div>
                <div className={"formCommentaries"}>
                  <div className="formCommentariesContainer" hidden={hiddenCommentaries}>
                    {commentaries.map((c: any, i: number) => {
                      if (c.idFilledForm === id) {
                        return <Commentary {...c} commentaryInputRef={commentaryInput} setCommentary={setCommentary} setIdCommentary={setIdCommentary} deletedCommentary={setCommentaryDeleted} />;
                      }
                    })}
                    {localStorage.getItem("isLogged") === "true" ? <div className="commentaryEnter">
                      <div className="commentaryInputContainer">
                        <input ref={commentaryInput} onChange={handleInputChange} onKeyDown={handleCommentaryKeyDown} className="inputCommentary commentary" type="text" value={commentary} placeholder={"Ecrivez votre commentaire"} />
                      </div>
                      { idCommentary !== null ? <div className="cancelCommentaryEdit" tabIndex={0} onKeyDown={handleCancelCommentaryEditKeyDown} aria-label="Annuler la modification de votre commentaire" onClick={cancelCommentaryEdit}><IonIcon tabIndex={0} name="close-circle-outline" className="iconCancelCommentaryEdit" src={cancel}/></div> : null}
                      <div className="commentaryButtonContainer">
                        <button className="commentarySendButton" name={"Envoyer"} onClick={createCommentary}>Envoyer</button>
                      </div>
                      
                    </div>
                    : null}
                  </div>
                </div>
                
              </div></>
            }
          })
        })
          : null
      }
    
    </div>
  );
};

export default FormDetails;
