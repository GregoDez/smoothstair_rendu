/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : index.ts
 * Description : Permet d'alléger les imports dans le projet.
 ************************************************************************/

import Button from "./Buttons/Button";
import CategorieCard from "./cards/CategorieCard";
import PlaceCard from "./cards/PlaceCard";
import PlaceHeader from "./headers/PlaceHeader";
import SearchHeader from "./headers/SearchHeader";
import Input from "./inputs/Input";
import InputArea from "./inputs/InputArea";
import SearchBar from "./inputs/SearchBar";
import Menu from "./Menu";
import HrSeparator from "./other/HrSeparator";
import Commentary from "./forms/Commentary";

import FormQuestionnary from "./forms/FormQuestionnary";
import RadiosGroup from "./forms/RadiosGroup";
import QuestionRow from "./forms/QuestionRow";
import Radio from "./forms/Radio";

export {
  Button,
  CategorieCard,
  PlaceCard,
  PlaceHeader,
  SearchHeader,
  Input,
  InputArea,
  SearchBar,
  Menu,
  HrSeparator,
  FormQuestionnary,
  Radio,
  RadiosGroup,
  QuestionRow,
  Commentary,
};
