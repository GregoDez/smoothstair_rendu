/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : PlaceCard.tsx
 * Description : Composant carte établissement
 ************************************************************************/

import { IonIcon, useIonRouter, IonCard } from "@ionic/react";
import "./PlaceCard.css";
import "../../theme/icons.css";
import { banner } from "../../icons";
import { Place } from "../../typescript/types/Place";
import { useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import {
  wheelChair as wheelChair,
  blind as blind,
  awesome,
  good,
  satisfaisant,
  bof,
  angry,
  nonRenseigner,
} from "../../icons/";

const PlaceCard = ({id, name, address, latitude, longitude, fontFamily }: any) => {
  const [disabilities, setDisabilities] = useState([]);
  const [accessibility, setAccessibility] = useState([]);
  const [notations, setNotations] = useState([]);
  let notationTitle = ""

  useEffect(() => {
    if(disabilities.length === 0 && path[1] !== "describe") {
      fetch('https://api.smoothstair.com/disabilities')
       .then((response) => response.json())
       .then((data) => {
          
        setDisabilities(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
    }
    
  }, []);

  useEffect(() => {
    if(notations.length === 0 && path[1] !== "describe") {
      fetch('https://api.smoothstair.com/notations')
       .then((response) => response.json())
       .then((data) => {
          
        setNotations(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
    }
  }, [])

  useEffect(() => {
    if(accessibility.length === 0) {
      if(id !== null && id !== undefined) {
        fetch('https://api.smoothstair.com/placesAccessibilities/byPlaceId/' + id)
         .then((response) => response.json())
         .then((data) => {
            
          setAccessibility(data);
         })
         .catch((err) => {
            console.log(err.message);
         });
      }
    }
  }, [])

  const router = useIonRouter();
  const history = useHistory();


  let srcIcon: string | undefined;
  let notationIcon: string | undefined;
  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");

  let href = "";
  let data = {
    
  }

  if (path[1] === "home") {
    href = "/home/" + path[2] + "/" + path[3] + "/" + id;
  } else if (path[1] === "describe") {
    href = "/describe/situation";

    if(path[3] !== "search") {
      data = {
        idPlace: id,
        name: name,
        address: address,
        latitude: latitude,
        longitude: longitude
      }
    }
    
  }

  const changePath = () => {
    history.push({ pathname: href, state: data });
  }

  const handleKeyDown = (event: { key: string; }) => {
    if (event.key === 'Enter') {
      // 👇️ your logic here
      changePath();
    }
  };


  return (
    <IonCard onClick={changePath} onKeyDown={handleKeyDown} aria-label={"Voir le détail du lieu " + name} className={"placeCardContainer " + fontFamily.fontFamily}>
      <div className="images">
        <img tabIndex={-1} id="banner" src={banner} />
        {/*<img className="placeImg" src={img_header_url} alt="" />*/}
      </div>
      <div className="placeDetails">
        <div className="placeDetail placeName">
          <span tabIndex={0}>{name}</span>
        </div>
        <div className="placeDetail" tabIndex={0}>
          <span>
            <b >Adresse : </b>
          </span>
          <span>{address}</span>
        </div>
        {/*<div className="placeDetail" tabIndex={0}>
          <span>
            <b>Distance : </b>
          </span>
  <span>{distance} mètres</span>
        </div>*/}
      </div>
      <div className="placeAccesibilities">

      {
                  
        disabilities.map((disability: any, i: number) => {
          if(path[1] !== "describe")  {

          
          switch (disability.id) {
            case 1:
              srcIcon = wheelChair;
              break;
            case 2:
              srcIcon = blind;
              break;
                  
            default:
              srcIcon = wheelChair;
              break;
          }

          return (
            <div key={i}>
              <div>
                <IonIcon tabIndex={0} aria-label={"handicap : " + disability.type} id="ouiiii" className="access" src={srcIcon} />
              </div>
              <div className="placeNotations">
              {
                
                accessibility.map((item: any) => {
                  notations.map((n: any) => {
                    if(item.idNotation === n.id) {
                      notationTitle = n.name
                    }
                  })
                    switch (item.idNotation) {
                      case 1:
                        
                        notationIcon = awesome;
                        break;
                      case 2:
                        notationIcon = good;
                        break;
                      case 3:
                        notationIcon = satisfaisant;
                        break;
                      case 4:
                        notationIcon = bof;
                        break;
                      case 5:
                        notationIcon = angry;
                        break;
                      case 6:
                        notationIcon = nonRenseigner;
                        break;
                      default:
                        notationIcon = nonRenseigner;
                        break;
                    }

                  if (disability.id === item.idDisability && item.idPlace === id)  {
                    return <IonIcon tabIndex={0} id="ouiiii" aria-label={"notation : " + notationTitle} className="notationIcon" src={notationIcon} />
                  } 

                  
                })
                
              }
              </div>
            </div>
          )
            }
        })   
      }
      </div>
    </IonCard>
  );
};

export default PlaceCard;
