/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : AccessCard.tsx
 * Description : Composant carte accessibilité
 ************************************************************************/

import { IonIcon, useIonRouter, IonCard } from "@ionic/react";
import "./AccessCard.css";
import "../../theme/icons.css";
import { useHistory } from "react-router-dom";
import { LocationDescriptor } from "history";
import { useEffect, useState } from "react";

/* Custom Icons */


/* Déclarations */


const AccessCard = ( { place, idDisability, disabilty, disabilityIcon, accessIcon, idNotation, fontFamily }: any ) => {
  const [notations, setNotations] = useState([]);
  const [newPath, setNewPath] = useState("");
  
  const router = useIonRouter();
  const history = useHistory();

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");
  let notationTitle = "";

  let data = {}
  
  
  useEffect(() => {
    
      fetch('https://api.smoothstair.com/notations')
      .then((response) => response.json())
      .then((data) => {
            
        setNotations(data);
      })
      .catch((err) => {
        console.log(err.message);
      });

      
      if(idNotation === 6) {
        setNewPath("/describe/situation")
      } else {
        setNewPath("/home/" + path[2] + "/" + path[3] + "/" + path[4] + "/" + idDisability)
      }
      
    
    
  }, []);

  const changePath = () => {
    data = {
      idPlace: place.id,
      name: place.name,
      address: place.address,
      latitude: place.latitude,
      longitude: place.longitude
    }
    history.push({pathname: newPath, state: data});
  }

  const handleKeyDown = (event: { key: string; }) => {

    // console.log(message);

    if (event.key === 'Enter') {
      // 👇️ your logic here
      changePath();
    }
  };

  notations.map((n: any, i: number) => {
    if(n.id === idNotation) {
      notationTitle = n.name;
    }
  })

  return (
    <IonCard onClick={changePath} onKeyDown={handleKeyDown} className={"accessCardContainer " + fontFamily.fontFamily}>
        <IonIcon id={disabilty} class="icons iconsFill" src={disabilityIcon} />
        <div tabIndex={0} className={"accessCardText"}>{disabilty}</div>
        <IonIcon id={disabilty} tabIndex={0} aria-label={"Notation : " + notationTitle} class="icons notationIcon" src={accessIcon} />
    </IonCard>  

  
  );
};

export default AccessCard;
