/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : CategorieCard.tsx
 * Description : Composant carte catégorie
 ************************************************************************/

import { IonIcon, useIonRouter, IonCard } from "@ionic/react";

import "./CategorieCard.css";
import "../../theme/icons.css";
import { useHistory } from "react-router-dom";
import { LocationDescriptor } from "history";

import {
  home as defaultIcon,
  health as healthIcon,
  commerce as commerceIcon,
  gestion as gestionIcon,
  resto as restoIcon,
  fun as funIcon,
  business as businessIcon,
} from "../../icons/";
import { useEffect } from "react";

const CategorieCard = ({ id, name, idCategory, fontFamily }: any) => {
  const router = useIonRouter();
  const history = useHistory();

  let srcIcon;

  let currentPath = router.routeInfo.pathname;

  let newPath: LocationDescriptor<unknown>;

  if (currentPath == "/home") {
    newPath = "/home/" + id;

    switch (id) {
      case 1:
        srcIcon = commerceIcon;
        break;
      case 2:
        srcIcon = gestionIcon;
        break;
      case 3:
        srcIcon = healthIcon;
        break; 
      case 4:
        srcIcon = businessIcon;
        break; 
      case 5:
        srcIcon = restoIcon;
        break;
      case 6:
        srcIcon = funIcon;
        break;

      default:
        break;
    }
  } else {
    newPath = "/home/" + idCategory + "/" + id;

    switch (idCategory) {
      case 1:
        srcIcon = commerceIcon;
        break;
      case 2:
        srcIcon = gestionIcon;
        break;
      case 3:
        srcIcon = healthIcon;
        break; 
      case 4:
        srcIcon = businessIcon;
        break; 
      case 5:
        srcIcon = restoIcon;
        break;
      case 6:
        srcIcon = funIcon;
        break;

      default:
        break;
    }
  }

  

  const changePath = () => {
    history.push(newPath);
  };

  const handleKeyDown = (event: { key: string; }) => {
    if (event.key === 'Enter') {
      // 👇️ your logic here
      changePath();
    }
  };  
  

  return (
    <IonCard  tabIndex={0} onKeyDown={handleKeyDown} aria-label={"Voir la catégorie " + name} onClick={changePath} className={"categorieCardContainer " + fontFamily.fontFamily}>
      <div>
        <IonIcon id={name} class="categorieIcons" src={srcIcon} />
      </div>
      <div className={"categorieCardText"}>{name}</div>
    </IonCard>
  );
};

export default CategorieCard;
