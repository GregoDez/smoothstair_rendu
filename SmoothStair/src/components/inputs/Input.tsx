/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : Input.tsx
 * Description : Composant Input
 ************************************************************************/
import "./Input.css";

interface Input {
  placeholder: string;
  classes: string;
}

const Input: React.FC<Input> = ({ placeholder, classes }) => {
  return (
    <div>
      <input tabIndex={0} type="text" className={classes} placeholder={placeholder} />
    </div>
  );
};

export default Input;
