/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : InputArea.tsx
 * Description : Composant Input Area
 ************************************************************************/
import "./InputArea.css";

interface InputArea {
  placeholder: string;
}

const InputArea: React.FC<InputArea> = ({ placeholder }) => {
  return (
    <div>
      <textarea name="textArea" className="textArea" placeholder={placeholder}></textarea>
    </div>
  );
};

export default InputArea;
