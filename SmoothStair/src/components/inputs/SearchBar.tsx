/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : SearchBar.tsx
 * Description : Composant barre de recherche
 ************************************************************************/
import { IonIcon } from "@ionic/react";

import "./SearchBar.css";

import SearchIcon from "../../icons/search.svg";

interface Props {
  onChange: any;
  hidden: boolean;
}

const SearchBar = (props: Props) => {
  const { hidden, onChange } = props;

  return (
    <div hidden={hidden}>
      <input onChange={onChange} tabIndex={0} type="search" className="searchbar" placeholder="Rechercher un établissement" />
      <IonIcon class="searchIcon" src={SearchIcon} />
    </div>
  );
};

export default SearchBar;
