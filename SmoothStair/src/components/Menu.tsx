/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : Menu.tsx
 * Description : Composant menu
 ************************************************************************/

import { IonButtons, IonContent, IonHeader, IonItem, IonLabel, IonList, IonMenu, IonMenuButton, IonPage, IonRouterOutlet, IonSplitPane, IonTitle, IonToolbar } from "@ionic/react";
import "./Menu.css";
import { useHistory } from "react-router";

const Menu: React.FC = () => {
  const history = useHistory();
  const deconnect = () => {
    localStorage.removeItem("isLogged");
    localStorage.removeItem("idUser");
    localStorage.removeItem("idRole");

    history.push("/home");
  }

  return (
    <>
    <IonMenu className="sideMenu" contentId="main-content">
        <IonHeader>
          <IonToolbar>
            <IonTitle className="whiteText primaryFont">Menu</IonTitle>
            
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">
        <IonList className="menuList primaryFont">
          <IonItem href="home">
            <a className="whiteText">Accueil</a>
          </IonItem>
          <IonItem href="/describe">
            <a className="whiteText">Renseigner un lieu</a>
          </IonItem>
          <IonItem href="/profil">
            <a className="whiteText">Profil</a>
          </IonItem>
          <IonItem href="/us">
            <a className="whiteText">Vous et nous</a>
          </IonItem>
          {
            localStorage.getItem('isLogged') === "true" ? <IonItem onClick={deconnect} href="/login">
              <a  className="whiteText">Déconnexion</a>
            </IonItem>
            :
            <IonItem href="/login">
              <a className="whiteText">Login</a>
            </IonItem>
          }
          {
            localStorage.getItem('isLogged') === "false" || localStorage.getItem('isLogged') === null  ? <IonItem href="/signup">
              <a className="whiteText">S'inscrire</a>
            </IonItem>
            : null
          }
          
        </IonList>
        </IonContent>
      </IonMenu>
      <IonPage className="IonPage" id="main-content">
        <IonHeader >
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton aria-label="Menu, cliquez pour ouvrir le menu" className="whiteText"></IonMenuButton>
            </IonButtons>
            <div className="menuTextContainer">
              <IonTitle className="whiteText">Menu</IonTitle>
              <div className="appName">SmoothStair</div>
            </div>
            
          </IonToolbar>
        </IonHeader>
      </IonPage>
    </>
    
  );
};

export default Menu;
