/************************************************************************
 * Auteur : Venezia Benjamin, Mendez Grégory
 * Fichier : PlaceHeader.tsx
 * Description : Composant pour un lieu en particulier.
 ************************************************************************/
import { Place } from "../../typescript/types/Place";
import "./PlaceHeader.css";
import { IonIcon, IonButtons, IonBackButton, useIonRouter, IonHeader } from "@ionic/react";
import { title } from "process";
import HrSeparator from "../other/HrSeparator";
import { useEffect, useState } from "react";
import { useLocation } from "react-router";

import {
  wheelChair as wheelChair,
  blind as blind,
  awesome,
  good,
  satisfaisant,
  bof,
  angry,
  nonRenseigner,
} from "../../icons/";

const PlaceHeader = (props: any) => {
  const { name, address, distance, img_header_url, type } = props.place;
  const hasFooter = props.hasFooter;
  const fontFamily = props.fontFamily

  const [disabilities, setDisabilities] = useState([]);

  const [notations, setNotations] = useState([]);

  const [accessibility, setAccessibility] = useState([]);

  const router = useIonRouter();
  let location = useLocation();

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");
  let srcIcon;
  let disabilityIcon: any, notationIcon: any;
  let notationName = "";
  


  useEffect(() => {
    fetch('https://api.smoothstair.com/disabilities')
       .then((response) => response.json())
       .then((data) => {
          
        setDisabilities(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/notations')
       .then((response) => response.json())
       .then((data) => {
          
        setNotations(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/placesAccessibilities/byPlaceId/' + path[4])
       .then((response) => response.json())
       .then((data) => {
          
        setAccessibility(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    if(path.length >= 5) 
      document.getElementById("inputTest")?.focus();
  }, [location]);


  return (
    <div className={fontFamily.fontFamily} id="content">
      <div  className="PlaceHeader__container">
        <IonButtons >
          <IonBackButton aria-label={"Retour"} defaultHref="{}" color="light" type="button" />
        </IonButtons>
        <div className="PlaceHeader__header">
          {/*<div className="PlaceHeader__leftcontainer">
            <img className="PlaceHeader__img" src={img_header_url} alt="" />
  </div>*/}
          <div className="PlaceHeader__infoscontainer">
            <h1 tabIndex={0}>{name}</h1>
            <p tabIndex={0}>Adresse: {address}</p>
            {/*<p tabIndex={0}>Distance: {distance} mètres</p>*/}
            {<p tabIndex={0} className="PlaceHeader__voirphotos" hidden>Voir les photos</p>}
          </div>
        </div>

        {hasFooter && (
          <div className="PlaceHeader__footer">
            <div className="PlaceHeader__footer__icons">
              {disabilities.map((disability: any, i: number) => {
                switch (disability.id) {
                  case 1:
                    srcIcon = wheelChair;
                    break;
                  case 2:
                    srcIcon = blind;
                    break;
                        
                  default:
                    srcIcon = wheelChair;
                    break;
                }
                if (disability.id === parseInt(path[5])) {
                  return <IonIcon tabIndex={0} aria-label={"Réponse pour la mobilité : " + disability.type} id="first" className="access" src={srcIcon} />;
                }
              })}
              {accessibility.map((item: any, i: number) => {
                if (item.idDisability === parseInt(path[5]) && item.idPlace === parseInt(path[4])) {
                  switch (item.idNotation) {
                    case 1:
                      notationIcon = awesome;
                      break;
                    case 2:
                      notationIcon = good;
                      break;
                    case 3:
                      notationIcon = satisfaisant;
                      break;
                    case 4:
                      notationIcon = bof;
                      break;
                    case 5:
                      notationIcon = angry;
                      break;
                    case 6:
                      notationIcon = nonRenseigner;
                      break;
                    default:
                      notationIcon = nonRenseigner;
                      break;
                  }
                  notations.map((notation: any, i: number) => {
                    if (notation.id === item.idNotation) {
                      notationName = notation.name;
                    }
                  });
                  
                  return <IonIcon tabIndex={0} id="first" aria-label={"notation : " + notationName} className="notationIcon" src={notationIcon} />;
                }
              })}
            </div>
            <div tabIndex={0} className="PlaceHeader__footer__map" hidden>
              <p >Afficher sur la carte: </p>
              <p >icon</p>
            </div>
          </div>
        )}
      </div>
      <HrSeparator />
    </div>
  );
};
export default PlaceHeader;
