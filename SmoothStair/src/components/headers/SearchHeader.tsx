/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : SearchHeader.tsx
 * Description : Composant carte catégorie
 ************************************************************************/

import { IonIcon, IonButton, useIonRouter, IonHeader, IonTitle, IonButtons, IonBackButton, IonLabel } from "@ionic/react";

/* Components */
import { SearchBar, HrSeparator, Menu } from "../.";

/* Custom import */

/* CSS */
import "./SearchHeader.css";
import "../../theme/icons.css";
import React, { useEffect, useState } from "react";

/* Déclarations */

interface header {
  type: number;
  inputText: String;
  handler: (e: any) => void;
  // 0 => HomePage
  // 1 => SubCategories
  // 2 => PlacesList
  // 3 => AddPlaces
}

const SearchHeader: React.FC<header> = ({ type, inputText, handler }) => {


  const [categories, setCategories] = useState([]);
  const [subcategories, setSubcategories] = useState([]);
  const router = useIonRouter();

  let currentPath = router.routeInfo.pathname;
  let path = currentPath.split("/");
  let title, href;
  let hiddenBackButton, hiddenSearchbar, hiddenHomeTitle, hiddenAddTitle, hiddenContainer, hiddenSeparator;
  hiddenContainer = false;
  hiddenSearchbar = false;

  
  useEffect(() => {
    fetch('https://api.smoothstair.com/categories')
       .then((response) => response.json())
       .then((data) => {
          
          setCategories(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/subcategories')
       .then((response) => response.json())
       .then((data) => {
          
          setSubcategories(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);
  

  switch (path.length) {
    case 2:
      type = 0;
      break;
    case 3:
      type = 1;
      break;
    case 4:
      type = 2;
      break;
    case 5:
      type = 3;
      break;
    case 6:
      type = 3;
      break;
    default:
      type = 0;
      break;
  }

  switch (type) {
    // Homepage
    case 0:
      hiddenBackButton = true;
      hiddenSearchbar = false;
      hiddenContainer = false;
      if (path[1] === "describe") {
        hiddenAddTitle = false;
        hiddenHomeTitle = true;
      } else {
        hiddenAddTitle = true;
        hiddenHomeTitle = false;
      }
      hiddenSeparator = false;
      break;

    // Subcategories
    case 1:
      hiddenContainer = false;
      hiddenSearchbar = false;
      hiddenBackButton = false;
      hiddenHomeTitle = true;
      hiddenAddTitle = true;
      hiddenSeparator = false;
      href = "/home";
      if (path[1] === "describe") {
        hiddenBackButton = true;
      } else {
        hiddenBackButton = false;
      }
      for (let i = 0; i < categories.length; i++) {
        const element = categories[i];
        if (element['id'] === parseInt(path[2])) {
          title = element['name'];
        }
      }
      break;

    // PlacesList
    case 2:
      hiddenContainer = false;
      hiddenSearchbar = false;
      hiddenBackButton = false;
      hiddenHomeTitle = true;
      hiddenAddTitle = true;

      hiddenSeparator = false;
      if (path[1] === "describe") {
        hiddenBackButton = true;
      } else {
        hiddenBackButton = false;
      }

      for (let i = 0; i < subcategories.length; i++) {
        const element = subcategories[i];
        if (element['id'] === parseInt(path[3])) {
          title = element['name'];
          href = "/home/" + element['idCategory'];
        }
      }
      break;

    case 3:
      hiddenContainer = true;
      hiddenSearchbar = true;
      hiddenBackButton = true;
      hiddenHomeTitle = true;
      hiddenAddTitle = true;
      
      hiddenSeparator = false;
      break;

    default:
      hiddenContainer = false;
      hiddenSearchbar = true;
      hiddenBackButton = true;
      hiddenHomeTitle = true;
      hiddenAddTitle = true;
      hiddenSeparator = true;
      href = "/home";

      break;
  }

  return (
    <IonHeader className="primaryFont" id="ionHeader" hidden={hiddenSeparator}>
      
      <SearchBar hidden={hiddenSearchbar} onChange={handler} />
      <div className="titleContainer" hidden={hiddenContainer}>
        <IonButtons hidden={hiddenBackButton} >
          <IonBackButton aria-label="Retour" defaultHref={href} color="light" type="button" />
        </IonButtons>
        <h1 hidden={!hiddenHomeTitle} tabIndex={0}>
          {title}
        </h1>
        <h1 className="titleCentered " id="homeTitle" hidden={hiddenHomeTitle} tabIndex={0}>
          Où souhaitez-vous aller ?
        </h1>
      </div>
    </IonHeader>
  );
};

export default SearchHeader;
