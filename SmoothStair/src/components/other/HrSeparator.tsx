import "./HrSeparator.css";

/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : HrSeparator.tsx
 * Description : Composant de séparation visuelle.
 ************************************************************************/
const Separator = () => {
  return <hr className="separator" />;
};
export default Separator;
