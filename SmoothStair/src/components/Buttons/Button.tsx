/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : Button.tsx
 * Description : Composant bouton
 ************************************************************************/

import { useHistory } from "react-router-dom";
import "./Button.css";

interface ButtonName {
  name: string;
  newPath: string;
  disabled?: boolean;
  classes: string;
}

const Button: React.FC<ButtonName> = ({ name, newPath, disabled, classes,  }) => {
  const history = useHistory();

  const changePath = () => {
    history.push(newPath);
  };

  return (
    <button tabIndex={0} aria-label={name} disabled={false} onClick={changePath} className={classes}>
      {name}
    </button>
  );
};

export default Button;
