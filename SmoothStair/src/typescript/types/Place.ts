export type Disability = {
  id: number;
  icon: any;
  isFill: boolean;
};

export type Place = {
  id: number;
  idSubcategory: number;
  name: string;
  subCategory: string;
  address: string;
  distance: number;
  type?: string | null;
  img_header_url: string;
  accessibility: Array<Disability>;
};
