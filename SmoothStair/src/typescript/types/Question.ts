/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : Question.ts
 * Description : Type pour chaque objet du store.
 ************************************************************************/

export type Question = {
  id: number;
  idSubCategory: number;
  idDisability: number;
  title: string;
  answers: Array<string>;
};
