/************************************************************************
 * Author : Mendez Grégory
 * Fichier : App.tsx
 * Description : Fichier contenant le routeur front
 ************************************************************************/
import { Redirect, Route } from "react-router-dom";
import { IonApp, IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs, setupIonicReact } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import "./theme/icons.css";

/* Pages */
import { HomePage, ProfilPage, UsPage, SituationToDescribePage } from "./pages";

/* Custom Icons */
import { home as homeIcon, us, profil, addform } from "./icons";

/* Custom imports */
import { changeActiveTab } from "./utils/tools.js";
import NotFoundPage from "./pages/NotFoundPage";
import PlacesToAddPage from "./pages/form/PlacesToAddPage";
import ConnectPage from "./pages/ConnectPage";
import SignupPage from "./pages/SignupPage";

setupIonicReact();

function App() {
  return (
    <IonApp>
      <IonReactRouter>
        {/*<IonTabs>*/}
          <IonRouterOutlet className="ionContent">
            <Redirect exact from="/" to="/home" />
            <Route exact path="/home">
              <HomePage />
            </Route>
            <Route path="/describe">
              <PlacesToAddPage />
            </Route>
            <Route path="/profil">
              <ProfilPage />
            </Route>
            <Route path="/us">
              <UsPage />
            </Route>
            <Route path="/login">
              <ConnectPage />
            </Route>
            <Route path="/signup">
              <SignupPage />
            </Route>
            <Route component={NotFoundPage} />
          </IonRouterOutlet>
          {/*<IonTabBar slot="bottom">
            <IonTabButton aria-label="Aller à l'accueil" onClick={() => changeActiveTab(1)} tab="home" href="/home">
              <IonIcon id="home" class="tab-icons" src={homeIcon} />
            </IonTabButton>
            <IonTabButton aria-label="Aller renseigner un lieu" onClick={() => changeActiveTab(2)} tab="describe" href="/describe">
              <IonIcon id="placeTab" class="tab-icons" src={addform} />
            </IonTabButton>
            <IonTabButton aria-label="Aller au profil" onClick={() => changeActiveTab(3)} tab="profil" href="/profil">
              <IonIcon id="profil" class="tab-icons" src={profil} />
            </IonTabButton>
            <IonTabButton aria-label="Aller à la section Vous et nous" onClick={() => changeActiveTab(4)} tab="Us" href="/us">
              <IonIcon id="us" class="tab-icons" src={us} />
            </IonTabButton>
  </IonTabBar>*/}
        {/*</IonTabs>*/}
      </IonReactRouter>
    </IonApp>
  );
}

export default App;
