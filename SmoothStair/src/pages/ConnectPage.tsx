/************************************************************************
 * Auteur :
 * Fichier : ProfilPage.tsx
 * Description : Fichier contenant le profil de l'utilisateur
 ************************************************************************/

import { IonContent, IonHeader, IonPage, IonToolbar } from "@ionic/react";
import { Menu } from "../components";
import "./ConnectPage.css";
import { useHistory } from "react-router";
import { SetStateAction, useEffect, useRef, useState } from "react";

const ConnectPage: React.FC = () => {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errMessage, setErrMessage] = useState("");
  const errorMessageRef = useRef<HTMLDivElement>(null);

  let sha1 = require('sha1');

  let handleEmailChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setEmail(event.target.value); 
    setErrMessage("");
  }

  let handlePasswordChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setPassword(event.target.value); 
    setErrMessage("");
  }

  useEffect(() => {
    if(errMessage !== "") {
      errorMessageRef.current?.focus()
    }
  }, [errMessage])

  const login = () => {
    
    let informationCorrect = false;
    if(email !== "" && password !== "") {
      informationCorrect = true;
    } else {
      setErrMessage("Veuillez remplir tous les champs");
    }

    if(informationCorrect) {
      fetch('https://api.smoothstair.com/users/login', {
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify({
          email: email,
          password: sha1(password)
        }),
      })
      .then((response)  => response.json())
      .then((data) => {
        if(data !== "Incorrect credentials" && data !== "No user found") {
          localStorage.setItem("idUser", data.id)
          localStorage.setItem("idRole", data.idRole)
          localStorage.setItem("isLogged", "true")
          history.push("/home")
        } else if(data === "No user found"){
          setErrMessage("Email incorrect");
          // TODO : affiche un message d'erreur
        } else {
          setErrMessage("Mot de passe incorrect");
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
    }
    
  }

  return (
    <IonPage>
      <Menu />
      <IonContent className="primaryFont" fullscreen>
        {
          localStorage.getItem("isLogged") === "true" ? <div>
            <h1 className="titleConnect primaryFont">Vous êtes déjà connecté</h1>
          </div>
          :
          <div>
            <h1 className="subtitle">Vous voulez vous connecter ?</h1>
            <div className="titleConnect" tabIndex={0}>Connexion</div>
            {errMessage !== "" && <div ref={errorMessageRef} className="errorMessage" aria-label={"Message d'erreur : " + errMessage} tabIndex={0}>{errMessage}</div>}
            <div className="formConnect">
              <label tabIndex={0}>Email :</label>
              <input tabIndex={0} className="input" type="text" onChange={handleEmailChange} placeholder={"Entrez votre email"}  />
              <label tabIndex={0}>Mot de passe :</label>
              <input tabIndex={0} className="input" type="password" onChange={handlePasswordChange} placeholder={"Entrez votre mot de passe"}  />
              
              <div className="profilButtonContainer">
                <button onClick={login} className="profilButton" tabIndex={0}>Se connecter</button>
              </div>
              <div tabIndex={-1}  className="signUpText">Vous n'avez pas de compte ? <a tabIndex={0} aria-label="Cliquez ici pour vous inscrire" className="linkToSignup" href="/signup">Inscrivez-vous ici</a></div>
            </div>
          </div>
        }
      </IonContent>
    </IonPage>
  );
};

export default ConnectPage;
