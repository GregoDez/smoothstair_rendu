/************************************************************************
 * Auteur : Grégory Mendez
 * Fichier : CategoriesList.tsx
 * Description : Affiche les catégories.
 ************************************************************************/
import { IonContent } from "@ionic/react";
import { useEffect, useState } from "react";
import { CategorieCard } from "../../components";
import "../ProfilPage.css";

import { useHistory } from "react-router-dom";
const Profil = () => {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [pseudo, setPseudo] = useState("");
  const [idRole, setIdRole] = useState(0);
  const [role, setRole] = useState("");
  const [idDisability, setIdDisability] = useState(0);
  const [disability, setDisability] = useState("");
  
  const goToConnection = () => {
    history.push("/login")
  }

  const goToSignUp = () => {
    history.push("/signup")
  }

  const deconnect = () => {
    localStorage.removeItem("isLogged");
    localStorage.removeItem("idUser");
    localStorage.removeItem("idRole");

    history.push("/home");
  }

  const deleteUser = () => {
    fetch('https://api.smoothstair.com/users/' + localStorage.getItem("idUser"), {
      method: 'delete',
      headers: { 
        'Content-Type': 'application/json',
        'Accept' : 'application/json' 
      },
    })
    .then((response) => {response.json()})
    .then((data) => {
      deconnect();
      console.log(data)
    })
    .catch((err) => {
      console.log(err.message);
    });
  }

  useEffect(() => {
    fetch('https://api.smoothstair.com/users/' + localStorage.getItem("idUser"))
       .then((response) => response.json())
       .then((data) => {
        setPseudo(data.pseudo)
        setEmail(data.email)
        setIdRole(data.idRole)
        setRole(data.role)
        setIdDisability(data.idDisability)
        setDisability(data.disability)
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);


  return (
      <IonContent>
        <div className="profilContainer">
        {
          localStorage.getItem("isLogged") === "true" ? <div>
            <div className="titleProfilContainer ion-padding">
              <h1>Profil</h1> 
            </div>
            <div className="profilContainer">
              <h2 className="profilSubtitle" tabIndex={0}>Informations personnelles</h2>
              <div className="profilInfos" tabIndex={0}>Email : {email}</div>
              <div className="profilInfos" tabIndex={0}>Pseudo : {pseudo}</div>
              <div className="profilInfos" tabIndex={0}>Role : {role}</div>
              <div className="profilInfos" tabIndex={0}>Handicap : {disability === null ? "Aucun handicap" :  disability}</div>
            </div>
            <div className="linkContainer ion-padding">
                <a tabIndex={0} href="/profil/edit" >Modifier mon compte</a>
                <a tabIndex={0} onClick={deleteUser}>Supprimer mon compte</a>
                {localStorage.getItem("idRole") === "1" || localStorage.getItem("idRole") === "2" ? <a tabIndex={0} >gestion admin</a> : null}
            </div>
          </div> 
          :
          <div className="profilConnectContainer">
            <h1 className="titleConnect" tabIndex={0}>Vous n'êtes pas connecté</h1>
            <div className="subtitle" tabIndex={0}>Vous voulez vous connecter ?</div>
            <div className="profilButtonContainer">
              <button onClick={goToConnection} className="profilButton" tabIndex={0}>Se connecter</button>
              <button onClick={goToSignUp} className="profilButton" tabIndex={0}>S'inscrire</button>
            </div>
          </div>
        }
        </div>
        
      </IonContent>
      
  );
};

export default Profil;
