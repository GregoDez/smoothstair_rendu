/************************************************************************
 * Auteur : Grégory Mendez
 * Fichier : CategoriesList.tsx
 * Description : Affiche les catégories.
 ************************************************************************/
import { IonContent, IonItem, IonLabel, IonList } from "@ionic/react";
import { SetStateAction, useEffect, useRef, useState } from "react";
import { CategorieCard } from "../../components";
import "../ProfilPage.css";

import { useHistory } from "react-router-dom";
const EditProfil = () => {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [pseudo, setPseudo] = useState("");
  const [idRole, setIdRole] = useState(0);
  const [role, setRole] = useState("");
  const [idDisability, setIdDisability] = useState();
  const [disability, setDisability] = useState("");
  const [disabilities, setDisabilities] = useState([]);
  const [roles, setRoles] = useState([]);
  const [errMessage, setErrMessage] = useState("");
  const errorMessageRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if(errMessage !== "") {
      errorMessageRef.current?.focus()
    }
  }, [errMessage])

  useEffect(() => {
    fetch('https://api.smoothstair.com/users/' + localStorage.getItem("idUser"))
       .then((response) => response.json())
       .then((data) => {
        setPseudo(data.pseudo)
        setEmail(data.email)
        setIdRole(data.idRole)
        setRole(data.role)
        setIdDisability(data.idDisability)
        setDisability(data.disability)
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    if(roles.length === 0)
    {
      fetch('https://api.smoothstair.com/roles')
       .then((response) => response.json())
       .then((data) => {
          
        setRoles(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
    }
  }, [])

  useEffect(() => {
    if(disabilities.length === 0)
    {
      fetch('https://api.smoothstair.com/disabilities')
       .then((response) => response.json())
       .then((data) => {
          
        setDisabilities(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
    }
  }, [])

  const handleDisabilityChange = (event: { target: { value: SetStateAction<any>; }; }) => {
    setIdDisability(event.target.value)
  }

  const handleRoleChange = (event: { target: { value: SetStateAction<any>; }; }) => {
    setIdRole(event.target.value)
  }

  let handlePseudoChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setPseudo(event.target.value); 
  }

  const updateUser = () => {

    fetch('https://api.smoothstair.com/users/' + email, {
        method: 'put',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify({
          pseudo: pseudo,
          idRole: idRole,
          idDisability: idDisability,
        }),
      })
      .then((response)  => response.json())
      .then((data) => {
        if(data.id !== null || data.id !== undefined) {
          history.push("/profil")
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
  }

  return (
      <IonContent>
        <div className="profilContainer">
        
          <div>
            <div className="titleProfilContainer ion-padding">
              <h1>Modification du profil</h1> 
            </div>
            <div className="profilContainer">
              <h2 className="profilSubtitle">Informations personnelles</h2>
              <div className="inputLabel">Pseudo :</div>
              <input type="text" onChange={handlePseudoChange} className="profilInfos" placeholder={pseudo}/>
              {
                localStorage.getItem("idRole") === "1" || localStorage.getItem("idRole") === "2" ? <>
                  <div className="inputLabel">Role :</div>
                  <IonList className="selectListEdit ">
                    <IonItem>
                    <select className="selectEdit" onChange={handleRoleChange} value={idRole} placeholder="Choisissez une sous-catégorie">
                      {
                        roles.map((d: any, i: number) => {
                          return <option value={d.id}>{d.name}</option>
                        })
                      }
                    </select>
                    </IonItem>
                  </IonList>
                </> : null
              }
              <div className="inputLabel">Handicap :</div>
              <IonList className="selectListEdit">
                <IonItem>
                <select className="selectEdit" onChange={handleDisabilityChange} value={idDisability} placeholder="Choisissez une sous-catégorie">
                <option>Pas de mobilité réduite</option>
                  {
                    disabilities.map((d: any, i: number) => {
                      return <option value={d.id}>{d.type}</option>
                    })
                  }
                </select>
                </IonItem>
              </IonList>
              <div className="editButtonContainer">
                <button onClick={updateUser} className="profilButton">Modifier</button>
              </div>
              
            </div>
          </div> 
        
        </div>
        
      </IonContent>
      
  );
};

export default EditProfil;
