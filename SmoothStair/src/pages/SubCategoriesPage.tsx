/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : SubCategoriesPage.tsx
 * Description : Fichier contenant la liste des sous catégories
 ************************************************************************/
import { IonContent, IonHeader, IonPage, IonToolbar, useIonRouter } from "@ionic/react";
import { useEffect, useState } from "react";
import { Menu, CategorieCard, SearchHeader } from "../components";

import "./SubCategoriesPage.css";

const SubCategoriesPage = () => {
  const [subcategories, setSubcategories] = useState([]);
  const [inputText, setInputText] = useState("");

  useEffect(() => {
    fetch('https://api.smoothstair.com/subcategories')
       .then((response) => response.json())
       .then((data) => {
        
          setSubcategories(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  let inputHandler = (e: any) => {
    var lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
  };

  const router = useIonRouter();

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");

  
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <Menu />
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <SearchHeader type={1} inputText={inputText} handler={inputHandler} />

        <div className="categoriesCardsContainer">

        
        </div>
      </IonContent>
    </IonPage>
  );
};

export default SubCategoriesPage;
