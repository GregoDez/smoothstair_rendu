/************************************************************************
 * Auteur :
 * Fichier : ProfilPage.tsx
 * Description : Fichier contenant le profil de l'utilisateur
 ************************************************************************/

import { IonContent, IonHeader, IonPage, IonToolbar } from "@ionic/react";
import { Menu } from "../components";
import "./ConnectPage.css";
import { useHistory } from "react-router";
import { SetStateAction, useEffect, useRef, useState } from "react";

const ConnectPage: React.FC = () => {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [pseudo, setPseudo] = useState("");
  const [password, setPassword] = useState("");
  const [confirmationPassword, setConfirmationPassword] = useState("");
  const [errMessage, setErrMessage] = useState("");
  const errorMessageRef = useRef<HTMLDivElement>(null);
  const emailRegex = new RegExp(/^[A-Za-z0-9_!#$%&'*+\/=?`{|}~^.-]+@[A-Za-z0-9.-]+$/, "gm");

  let sha1 = require('sha1');

  useEffect(() => {
    if(errMessage !== "") {
      errorMessageRef.current?.focus()
    }
  }, [errMessage])

  let handleEmailChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setEmail(event.target.value); 
    setErrMessage("");
  }

  let handlePseudoChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setPseudo(event.target.value); 
    setErrMessage("");
  }

  let handlePasswordChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setPassword(event.target.value); 
    setErrMessage("");
  }

  let handleConfirmationPasswordChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setConfirmationPassword(event.target.value); 
    setErrMessage("");
  }

  const signup = () => {
    let informationCorrect = false;
    const isValidEmail = emailRegex.test(email)

    

    if(email === "" || pseudo === "" || password === "" || confirmationPassword === "") {
      informationCorrect = false;
      setErrMessage("Veuillez remplir les champs");
    } else {
      if(password === confirmationPassword) {
        informationCorrect = true;
      } else {
        informationCorrect = false;
        setErrMessage("Le mot de passe est différent de la confirmation");
      }
    }

    if(!isValidEmail) {
      informationCorrect = false;
      setErrMessage("Vérifier que votre adresse email est valide");
    }

    if(informationCorrect) {
      fetch('https://api.smoothstair.com/users/signup', {
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        },
        body: JSON.stringify({
          email: email,
          pseudo: pseudo,
          password: sha1(password),
          confirmationPassword: sha1(confirmationPassword),
          idRole: 4,
          idDisability: null,
        }),
      })
      .then((response)  => response.json())
      .then((data) => {
        if(data != "User already exist with this email ") {
          history.push("/login")
        } else {
          setErrMessage("Cet email est déjà utilisé");
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
    }
  }

  return (
    <IonPage>
      <Menu />
      <IonContent  className="primaryFont" fullscreen>
        {
          localStorage.getItem("isLogged") === "true" ? <div>
            <h1 className="titleConnect">Vous êtes déjà connecté</h1>
          </div>
          :
          <div>
            <div className="titleConnect">Connexion</div>
            {errMessage !== "" && <div ref={errorMessageRef} className="errorMessage" aria-label={"Message d'erreur : " + errMessage} tabIndex={0}>{errMessage}</div>}
            <div className="formConnect">
              <label tabIndex={0}>Email* :</label>
              <input tabIndex={0} className="input" type="text" onChange={handleEmailChange} placeholder={"Entrez votre email"}  />
              <label tabIndex={0}>Pseudo* :</label>
              <input tabIndex={0} className="input" type="text" onChange={handlePseudoChange} placeholder={"Entrez votre mot de passe"}  />
              <label tabIndex={0}>Mot de passe* :</label>
              <input tabIndex={0} className="input" type="password" onChange={handlePasswordChange} placeholder={"Entrez votre mot de passe"}  />
              <label tabIndex={0}>Confirmation du mot de passe* :</label>
              <input tabIndex={0} className="input" type="password" onChange={handleConfirmationPasswordChange} placeholder={"Entrez votre mot de passe"}  />
              <div className="profilButtonContainer">
                <button tabIndex={0} onClick={signup} className="profilButton">S'inscrire</button>
              </div>
              <div tabIndex={-1} className="signUpText">Vous avez déjà un compte ? <a  tabIndex={0} aria-label="Cliquez ici pour vous connecter" className="linkToSignup" href="/login">Connectez-vous ici</a></div>
            </div>
          </div>
        }
      </IonContent>
    </IonPage>
  );
};

export default ConnectPage;
