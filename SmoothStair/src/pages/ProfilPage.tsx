/************************************************************************
 * Auteur :
 * Fichier : ProfilPage.tsx
 * Description : Fichier contenant le profil de l'utilisateur
 ************************************************************************/

import { IonContent, IonHeader, IonPage, IonRouterOutlet, IonToolbar } from "@ionic/react";
import { Menu } from "../components";
import { Route, useHistory } from "react-router";
import Profil from "./profil/Profil";
import EditProfil from "./profil/EditProfil";

const ProfilPage: React.FC = () => {

  return (
    <IonPage>
      <Menu />
      <IonContent>
        
        <IonRouterOutlet>
          <Route exact path="/profil">
            <Profil />
          </Route>
          <Route exact path="/profil/edit">
            <EditProfil />
          </Route>
        </IonRouterOutlet>
      </IonContent>
    </IonPage>
  );
};

export default ProfilPage;
