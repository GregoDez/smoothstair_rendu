/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : placesToAddPage.tsx
 * Description : étape 1/4 du remplissage de formulaire d'un lieu.
 ************************************************************************/

import {
  IonBackButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonLabel,
  IonPage,
  IonRouterOutlet,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { Route, useHistory, useLocation } from "react-router-dom";
import { HrSeparator, Menu } from "../../components";
import PlacesList from "../home/PlacesList";
import SituationsList from "./SituationsList";
import SearchIcon from "../../icons/search.svg";

/* CSS */
import "./PlacesToAddPage.css";
import Questionnary from "./Questionnary";

const PlacesToAddPage: any = () => {

  const history = useHistory();
  const router = useIonRouter();
  const [inputAddText, setInputAddText] = useState("");
  let location = useLocation();

  useEffect(() => {
    document.getElementById("searchBar")?.focus();
  }, [location]);

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");

  let hiddenBackButton = true;
  let type = 0;
  let href = "";

  useEffect(() => {
    if (inputAddText !== "") {
      const timer = setTimeout(() => {
        history.push("/describe/search/" + inputAddText);
      }, 500)
      return () => clearTimeout(timer)
    } else if(inputAddText === "" && path[2] === "search" ) {
      history.push("/describe/search/" + inputAddText);
    }
  }, [inputAddText]);

  let inputHandler = (e: any) => {
    let lowerCase = e.target.value.toLowerCase();
    setInputAddText(lowerCase);
    
    
  };

  switch (path.length) {
    case 2:
      type = 0;
      hiddenBackButton = true;
      break;
    case 3:
      type = 1;
      hiddenBackButton = false;
      break;
    case 4:
      hiddenBackButton = true;
      if (path[2] === "search") {
        type = 0;
      } else {
        type = 2;
      }
      break;
    case 5:
      if (path[4] === "comment") {
        type = 7;
      }
      break;
    default:
      type = 0;
      break;
  }

  

  return (
    <IonPage>
        <Menu />
      <IonContent>
      
        {
          localStorage.getItem("isLogged") === "true" ? <IonHeader id="ionHeader"><div id="title" className="titleContainer">
            <IonButtons hidden={hiddenBackButton}>
              <IonBackButton aria-label="Retour" defaultHref={href} color="light" type="button" />
            </IonButtons>

            <h1 id="addTitle" className="primaryFont" tabIndex={0}>
              Aidez la communauté en <IonLabel color="secondary">renseignant</IonLabel> un lieu !
            </h1>
          </div>
          <div>
            <input
              id="searchBar"
              onChange={inputHandler}
              tabIndex={0}
              type="search"
              className="searchbar"
              placeholder="Rechercher un établissement"
            />
            <IonIcon class="searchIcon" src={SearchIcon} />
          </div>
          <HrSeparator />
          </IonHeader>
          :
          null
        }
          
        
        <IonRouterOutlet>
          <Route exact path="/describe" component={PlacesList} />
          <Route exact path="/describe/situation" component={SituationsList} />
          <Route exact path="/describe/situation/:idSubcategory/:idDisability" component={Questionnary} />
          <Route exact path="/describe/search/:text" component={PlacesList} />
        </IonRouterOutlet>
      </IonContent>
    </IonPage>
  );
};

export default PlacesToAddPage;
