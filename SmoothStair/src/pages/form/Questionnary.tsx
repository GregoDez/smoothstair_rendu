import { IonBackButton, IonButtons, IonContent, IonHeader, IonLabel, IonPage, useIonRouter } from "@ionic/react";
import { FormQuestionnary } from "../../components";
import { useEffect, useState } from "react";

const Questionnary = () => {
  const [subcategories, setSubcategories] = useState([]);
  const [disabilities, setDisabilities] = useState([]);

  useEffect(() => {
    fetch('https://api.smoothstair.com/subcategories')
       .then((response) => response.json())
       .then((data) => {
          
          setSubcategories(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/disabilities')
       .then((response) => response.json())
       .then((data) => {
          
        setDisabilities(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  const router = useIonRouter();

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");

  return (
    <IonContent fullscreen>
      <div className="formContainer">
        
        <h2 className="formTitle" tabIndex={0}>Formulaire des <IonLabel color="secondary">
          {
            subcategories.map((s: any, i: number) => {
              if(s.id === parseInt(path[3])) {
                return s.name
              }
            })}
          </IonLabel> pour <IonLabel color="secondary">
          {
            disabilities.map((d: any, i: number) => {
              if(d.id === parseInt(path[4])) {
                return d.type
              }
            })}
          </IonLabel>
        </h2>
        <FormQuestionnary />
      </div>
    </IonContent>
  );
};
export default Questionnary;
