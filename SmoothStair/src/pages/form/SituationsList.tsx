import { IonContent, IonButtons, IonBackButton, useIonRouter, IonList, IonItem, IonSelect, IonText, IonSelectOption, IonLabel } from "@ionic/react";
import { SetStateAction, useEffect, useState } from "react";
import { HrSeparator } from "../../components";

/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : SituationToDescribePage.tsx
 * Description : étape 2/4 du remplissage de formulaire d'un lieu.
 ************************************************************************/
import { Button } from "../../components";
import { useHistory, useLocation } from "react-router-dom";
import { userInfo } from "os";

const SituationsList = () => {
  const history = useHistory();
  const location = useLocation();
  const [subcategories, setSubcategories] = useState([]);
  const [disabilities, setDisabilities] = useState<any>([]);
  const [idSubCategory, setIdSubCategory] = useState("1");
  const [idDisability, setIdDisability] = useState("1");

  const data = location.state;
  
  useEffect(() => {
    if(subcategories.length === 0)
    {
      fetch('https://api.smoothstair.com/subcategories')
      .then((response) => response.json())
      .then((data) => {
         
         setSubcategories(data);
      })
      .catch((err) => {
         console.log(err.message);
      });
    }    
  }, []);

  useEffect(() => {
    if(disabilities.length === 0)
    {
      fetch('https://api.smoothstair.com/disabilities')
       .then((response) => response.json())
       .then((data) => {
          
        setDisabilities(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
    }
  }, [])

  const handleDisabilityChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setIdDisability(event.target.value)
  }

  const handleSubcategoryChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setIdSubCategory(event.target.value)

  }

  

  const changePath = () => {
    let newPath = "/describe/situation/" + `${idSubCategory}` + `/${idDisability}`

    history.push({ pathname: newPath, state: data });
  }

  return (
    <IonContent>
      <div className="formContainer">
        
        <h2 className="secondTitle" tabIndex={0}><IonLabel color="secondary">Type de mobilité</IonLabel> à décrire</h2>

        <IonList className="selectList">
          <IonItem>
            <select className="select" onChange={handleDisabilityChange} value={idDisability} placeholder="Choisissez une sous-catégorie">
              {
                disabilities.map((d: any, i: number) => {
                  return <option value={d.id}>{d.type}</option>
                })
              }
            </select>
          </IonItem>
        </IonList>
        <h2 tabIndex={0}>Veuillez selectionner le <IonLabel color="secondary">type de lieu</IonLabel> à renseigner</h2>
        
        <IonList className="selectList">
          <IonItem>
            <select className="select"  onChange={handleSubcategoryChange} value={idSubCategory} placeholder="Choisissez une sous-catégorie">
              {
                subcategories.map((s: any, i: number) => {
                  return <option value={s.id}>{s.name}</option>
                })
              }
            </select>
          </IonItem>
        </IonList>
        <button onClick={changePath}  className={"buttonContainer commonButton"} >Continue</button>
      </div>
    </IonContent>
  );
};
export default SituationsList;
