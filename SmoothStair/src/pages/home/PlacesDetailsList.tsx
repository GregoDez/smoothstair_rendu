/************************************************************************
 * Auteur : Grégory Mendez
 * Fichier : PlacesDetailsList.tsx
 * Description : Affiche la liste des répones au formulaire correspondant.
 ************************************************************************/
import { PlaceHeader } from "../../components";
import { IonContent, IonLabel, useIonRouter } from "@ionic/react";

/* CSS */
import "./HomePage.css";
import FormDetails from "../../components/forms/FormDetails";
import { useEffect, useState } from "react";

const PlacesDetailsList = (fontFamily: any) => {
  const [places, setPlaces] = useState([]);
  const [formReponses, setFormReponses] = useState([]);
  const [forms, setForms] = useState([]);
  const [idSubCategory, setIdSubCategory] = useState("");

  
  const router = useIonRouter();

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");
  
  useEffect(() => {
    fetch('https://api.smoothstair.com/places/' + path[4])
       .then((response) => response.json())
       .then((data) => {
          setPlaces(data);
          setIdSubCategory(data.idSubCategory)
         
       })
       .catch((err) => {
          console.log(err.message);
       });

       setIdSubCategory(path[3])
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/filledForms')
       .then((response) => response.json())
       .then((data) => {
        setFormReponses(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    
    fetch('https://api.smoothstair.com/forms/byIdSubCategory/' + parseInt(idSubCategory))
       .then((response) => response.json())
       .then((data) => {
        setForms(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, [idSubCategory]);



  return (
    <IonContent fullscreen>
      <div className="header">
        <PlaceHeader fontFamily={fontFamily} key={`header-${path[4]}`} hasFooter={true} place={places}/>
      </div>
      <h1 className={"placeDetailsFormTitle " + fontFamily.fontFamily} tabIndex={0}>
        {" "}
        Avis de la <IonLabel color="secondary">communauté</IonLabel>
      </h1>
      <div className="formDetailsContainer">
        {
          formReponses.map((item: any, i: number) => {
            return forms.map((f: any, j: number) => {
              if(item.idPlace === parseInt(path[4]) && f.idDisability === parseInt(path[5])) {
                return <FormDetails fontFamily={fontFamily} key={`${i}-${item.id}-${item.idform}-${item.idPlace}`} {...item} />  
              }
            })   
          })
        }
      </div>
    </IonContent>
  );
};
export default PlacesDetailsList;
