/************************************************************************
 * Auteur : Grégory Mendez
 * Fichier : CategoriesList.tsx
 * Description : Affiche les catégories.
 ************************************************************************/
import { IonContent } from "@ionic/react";
import { useEffect, useState } from "react";
import { CategorieCard } from "../../components";
import "./HomePage.css";

import { useHistory } from "react-router-dom";



const CategoriesList = (fontFamily: any) => {

  const [categories, setCategories] = useState([]);
  const [inputText, setInputText] = useState("");
  const history = useHistory();

  let inputHandler = (e: any) => {
    var lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
  };

  useEffect(() => {
    fetch('https://api.smoothstair.com/categories')
       .then((response) => response.json())
       .then((data) => {
          
          setCategories(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
       
  }, []);

  
  
  
  return (
      <IonContent className="">
        <div className="componentContainer">
          <div className="categoriesCardsContainer">
            {categories.map((item:any, i:any) => {

              // Return the element. Also pass key
              return <CategorieCard key={item.id} fontFamily={fontFamily} {...item} />;
            })}
          </div>
        </div>
        
      </IonContent>
      
  );
};

export default CategoriesList;
