/************************************************************************
 * Auteur : Grégory Mendez
 * Fichier : PlacesList.tsx
 * Description : Affiche les Lieux.
 ************************************************************************/
import { PlaceCard } from "../../components";
import { IonContent, useIonRouter } from "@ionic/react";

/* CSS */
import "./HomePage.css";
import { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router";
import { Wrapper, Status } from "@googlemaps/react-wrapper";



const PlacesList = (fontFamily: any) => {
  const [places, setPlaces] = useState<any>([]);
  const [filteredPlaces, setFilteredPlaces] = useState<any>([]);
  const [modifiedPlaces, setModifiedPlaces] = useState<any>(false);

  const history = useHistory();

  const router = useIonRouter();
  let currentPath = router.routeInfo.pathname;
  let path = currentPath.split("/");
  let counterPlacesShowed = 0;
  let style = "";
  
  
  //let geneva = new google.maps.LatLng(46.20447365445971, 6.144457024329831);
  let map = new google.maps.Map(document.createElement('div'));
  let service = new google.maps.places.PlacesService(map);
  
  useEffect(() => {
    setFilteredPlaces(places.filter((p: any) => {
      return p;
    }))
  }, [places])


  if (path[1] === "home") {
    if (path[2] === "search") {
      style = "placesContainer";
    } else {
      style = "subCategoriesPlacesContainer";
    }
  } else {
    style = "placesContainer";
  }

  useEffect(() => {
    if(path[3] !== "") {
      if(filteredPlaces.length !== 0) {
        setFilteredPlaces(places.filter((p: any) => {
          return p.name.toLowerCase().includes(path[3]);
        }))
      } else if (filteredPlaces.length === 0) {
        setFilteredPlaces(places.filter((p: any) => {
          return p;
        }))
      }
    }
    
    if(path[1] === "home" && path[2] === "search" && path[3] !== "") {
      

      let tmpArray : any = []

      tmpArray = places.filter((p: any) => {
        return p.name.toLowerCase().includes(path[3])
      })

      /*tmpArray.map((a: any, i: number) => {
        filteredPlaces.push({id: a.id, name: a.name, address: a.address})
      })*/
      setFilteredPlaces(tmpArray)
      
    }
    if(path[1] === "describe" && path[2] === "search" && path[3] !== "") {

      
      const request = {
        query: path[3],
        fields: ["place_id","name", "formatted_address", "geometry"],
      };

      let tmpArray : any = []      

      service.textSearch(request, (results, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK && results) {
          for (let i = 0; i < results.length; i++) {
            tmpArray.push({id: null, name: results[i].name, address: results[i].formatted_address, latitude: results[i].geometry?.location.lat(), longitude: results[i].geometry?.location.lng()})
            
          }
        }
        setFilteredPlaces(tmpArray)
        setModifiedPlaces(true)
      });
    }
  }, [path[3]])


  useEffect(() => {
    
    fetch('https://api.smoothstair.com/places')
       .then((response) => response.json())
       .then((data) => {
          
          setPlaces(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  const goToConnection = () => {
    history.push("/login")
  }

  let goToSignUp = () => {
    history.push("/signup")
  }
  
  return (
    <IonContent>
      <div id="map"></div>
      <div className={style + " " + fontFamily.fontFamily}>
        {
        localStorage.getItem("isLogged") != "true" && path[1] === "describe" ?
        <div>
          <h1 className="titleConnect">Vous n'êtes pas connecté</h1>
          <div className="subtitle">Vous voulez vous connecter ?</div>
          <div className="profilButtonContainer">
            <button onClick={goToConnection} className="profilButton">Se connecter</button>
            <button onClick={goToSignUp} className="profilButton">S'inscrire</button>
          </div>
        </div>
        :
        filteredPlaces
          //.sort((a, b) => (a.distance > b.distance ? 1 : -1))
          .map((p: any, i: number) => {
            if (path[2] === "search") {
              counterPlacesShowed++;
              return <PlaceCard fontFamily={fontFamily} key={`${i}-${p.id}-${p.name}-${p.address}`} {...p} />;
            } else if (p.idSubCategory === parseInt(path[3])) {
              counterPlacesShowed++;
              return <PlaceCard fontFamily={fontFamily} key={`${i}-${p.id}-${p.name}-${p.address}`} {...p} />;
            } else if (path[1] === "describe") {
                counterPlacesShowed++;
                return <PlaceCard fontFamily={fontFamily} key={`${i}-${p.id}-${p.name}-${p.address}`} {...p} />;
            }
          })
          
        }

        {localStorage.getItem("isLogged") === "true" ? counterPlacesShowed === 0 && <p className="ion-padding whiteText">Pas de lieu à afficher</p> : null}
      </div>
    </IonContent>
  );
};
export default PlacesList;

