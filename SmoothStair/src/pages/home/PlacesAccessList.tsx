/************************************************************************
 * Auteur : Grégory Mendez
 * Fichier : PlacesAccessList.tsx
 * Description : Affiche l'accessibilité par handicap d'un lieu spécifique.
 ************************************************************************/
import { PlaceHeader } from "../../components";
import { IonContent, useIonRouter } from "@ionic/react";

/* CSS */
import "./HomePage.css";
import AccessCard from "../../components/cards/AccessCard";
import { useEffect, useRef, useState } from "react";
import {
  wheelChair as wheelChair,
  blind as blind,
  awesome,
  good,
  satisfaisant,
  bof,
  angry,
  nonRenseigner,
} from "../../icons/";

const PlacesAccessList = (fontFamily: any) => {
  const [places, setPlaces] = useState([]);
  const [place, setPlace] = useState({});
  const [disabilities, setDisabilities] = useState([]);
  const [accessibility, setAccessibility] = useState([]);
  const [notations, setNotations] = useState([]);

  

  const router = useIonRouter();

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");
  let srcIcon: string;
  let disabilityIcon: any, notationIcon: any, disabilityTitle: string;


  useEffect(() => {
    fetch('https://api.smoothstair.com/places')
       .then((response) => response.json())
       .then((data) => {
          
          setPlaces(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    if(places.length !== 0) {
      places.map((item: any, i) => {
        if(item.id === parseInt(path[4])) {
          
          setPlace(item)
        }
      })
    }
    
  }, [places]);

  

  useEffect(() => {
    fetch('https://api.smoothstair.com/disabilities')
       .then((response) => response.json())
       .then((data) => {
          
        setDisabilities(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/notations')
       .then((response) => response.json())
       .then((data) => {
          
        setNotations(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  useEffect(() => {
    fetch('https://api.smoothstair.com/placesAccessibilities/byPlaceId/' + path[4])
       .then((response) => response.json())
       .then((data) => {
          
        setAccessibility(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  return (
      <IonContent>
        
        <div className="header">
        {
          places.map((item: any, i) => {
            if(item.id === parseInt(path[4])) {
              
              return <PlaceHeader fontFamily={fontFamily} place={item} />
            }
          })
        }
        </div>
        
        <div className="" >
        {
          accessibility.map((accessibility: any, i: number) => {
            if(accessibility.idPlace === parseInt(path[4])) {
              
        
              disabilities.map((disability: any, i: number) => {
                if(accessibility.idDisability === disability.id) {
                  switch (disability.id) {
                    case 1:
                      srcIcon = wheelChair;
                      break;
                    case 2:
                      srcIcon = blind;
                      break;
                          
                    default:
                      srcIcon = wheelChair;
                      break;
                  }
                  disabilityTitle = disability.type;
                  
                }
              })
        
              notations.map((notation: any, i: number) => {
                if(accessibility.idNotation === notation.id) {
                  notationIcon = notation.icon;
                  switch (accessibility.idNotation) {
                    case 1:
                      notationIcon = awesome;
                      break;
                    case 2:
                      notationIcon = good;
                      break;
                    case 3:
                      notationIcon = satisfaisant;
                      break;
                    case 4:
                      notationIcon = bof;
                      break;
                    case 5:
                      notationIcon = angry;
                      break;
                    case 6:
                      notationIcon = nonRenseigner;
                      break;
                    default:
                      notationIcon = nonRenseigner;
                      break;
                  }
                }
              })
        
              return <AccessCard place={place} fontFamily={fontFamily} key={`access-${i}-${accessibility.idDisability}-${accessibility.idNotation}-${accessibility.idPlace}`} idDisability={accessibility.idDisability} disabilty={disabilityTitle} idNotation={accessibility.idNotation} accessIcon={notationIcon} disabilityIcon={srcIcon} />
            }
          })

        }  
        </div>
      </IonContent>
  );
};
export default PlacesAccessList;
