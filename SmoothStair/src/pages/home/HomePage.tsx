/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : HomePage.tsx
 * Description : Fichier contenant la page d'accueil
 ************************************************************************/
import {
  IonBackButton,
  IonButtons,
  IonContent,
  IonFooter,
  IonHeader,
  IonIcon,
  IonPage,
  IonRouterOutlet,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import { createRef, useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";

import { Menu, CategorieCard, HrSeparator, SearchHeader } from "../../components";
import CategoriesList from "./CategoriesList";
import SubCategoriesList from "./SubCategoriesList";
import PlacesList from "./PlacesList";
import PlacesAccessList from "./PlacesAccessList";

import "./HomePage.css";

import { Route, useLocation } from "react-router";
import PlacesDetailsList from "./PlacesDetailsList";

const HomePage = () => {


  const history = useHistory();
  const router = useIonRouter();
  let location = useLocation();
  const [inputText, setInputText] = useState("");
  const [fontFamily, setFontFamily] = useState(" primaryFont");

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");

  useEffect(() => {
    if(path.length != 2)
      document.getElementById("searchBar")?.focus();
  }, [location]);

  useEffect(() => {
    
    if (inputText != "") {
      const timer = setTimeout(() => {
        history.push("/home/search/" + inputText);
      }, 500)
      return () => clearTimeout(timer)
    } else {
      history.push("/home");
    }
  }, [inputText]);

  let inputHandler = (e: any) => {
    var lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
  };

  


  return (
    <IonPage>
      <Menu/>
      
      <IonContent>
      <SearchHeader type={1} inputText={inputText} handler={inputHandler} />
        <IonRouterOutlet>
          <Route exact path="/home">
            <CategoriesList fontFamily={fontFamily} />
          </Route>
          <Route exact path="/home/:categorie">
            <SubCategoriesList fontFamily={fontFamily} />
          </Route>
          <Route exact path="/home/:categorie/:subCategorie">
            <PlacesList fontFamily={fontFamily}/>
          </Route>
          <Route exact path="/home/:categorie/:subCategorie/:place">
            <PlacesAccessList fontFamily={fontFamily}/>
          </Route>
          <Route exact path="/home/:categorie/:subCategorie/:place/:disability">
            <PlacesDetailsList fontFamily={fontFamily} />
          </Route>
          <Route exact path="/home/search/:text">
            <PlacesList />
          </Route>
        </IonRouterOutlet>
      </IonContent>
    </IonPage>
  );
};

export default HomePage;
