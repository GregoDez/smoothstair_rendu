/************************************************************************
 * Auteur : Grégory Mendez
 * Fichier : SubCategoriesList.tsx
 * Description : Affiche les sous-catégories selon l'id catégorie
 ************************************************************************/
import { IonContent, useIonRouter } from "@ionic/react";
import { useEffect, useState } from "react";
import { CategorieCard } from "../../components";

const SubCategoriesList = (fontFamily: any) => {
  const [subcategories, setSubcategories] = useState([]);
  const [inputText, setInputText] = useState("");

  let inputHandler = (e: any) => {
    var lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
  };

  const router = useIonRouter();

  let currentPath = router.routeInfo.pathname;

  let path = currentPath.split("/");

  useEffect(() => {
    fetch('https://api.smoothstair.com/subcategories')
       .then((response) => response.json())
       .then((data) => {
          
          setSubcategories(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
  }, []);

  return (
    <IonContent>
      <div className="componentContainer">
        <div className="categoriesCardsContainer">
          {subcategories.map((item: any, i: any) => {
            if (item.idCategory === parseInt(path[2])) {
              // Return the element. Also pass key
              return <CategorieCard fontFamily={fontFamily} key={item.id} {...item} />;
            }
          })}
        </div>
      </div>
    </IonContent>
  );
};

export default SubCategoriesList;
