/************************************************************************
 * Auteur : Mendez Grégory
 * Fichier : UsPage.tsx
 * Description : Fichier contenant les informations de projet
 ************************************************************************/
import { IonContent, IonHeader, IonLabel, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import React, { useRef } from "react";
import { useEffect } from "react";

import { Menu, Button, Input, InputArea } from "../components";
import "./UsPage.css";

const UsPage: React.FC = () => {

  const headingRef = useRef(0)

  useEffect(() => {
    headingRef.current = 0;
  }, []);
  return (
    <IonPage>
          <Menu />
      <IonContent className="primaryFont">
        <section className="usContainer">
          <h1 className="title" tabIndex={0}>
            Avec <IonLabel color="secondary">SmoothStair</IonLabel>, on pense à <IonLabel>vous</IonLabel> !
          </h1>
          <div className="separator"></div>
          <div className="description">
            <p>Nous sommes une équipe de 4 personnes située à Genève en Suisse. Nous sommes sensibles aux problématiques humaines. </p>
            <p>Le projet SmoothStair est né d’un constat : il n y a aucune alternative viable dans le monde de la mobilité réduite. </p>
            <p>
              Nous avons décidé de penser à une solution qui permettent aux gens qui en ont besoin d’avoir un aperçu complet en fonction de
              son handicap.{" "}
            </p>
            <p>Nous travaillons avec des professionnels pour nous assurer de la qualité du traitement des données.</p>
          </div>
        </section>
        {/*<section>
          <h2 className="title" tabIndex={0}>
            Détails du <IonLabel color="secondary">développement</IonLabel>
          </h2>
          <div className="separator"></div>
          <p className="description">
            Nous cherchons à couvrir le plus de cas de mobilité réduite que possible, vous pouvez connaître les dates d’ajout via cet outil
            accessible aux personnes malvoyantes:
          </p>
        </section>
        <section>
          <h2 className="title" tabIndex={0}>
            Nous <IonLabel color="secondary">soutenir</IonLabel>
          </h2>
          <div className="separator"></div>
          <p className="description">
            La philosophie SmoothStair c’est avant toute chose un service totalement gratuit pour la communauté. Vous pouvez nous soutenir
            et soutenir le développement de la plateforme en passant par une donation. Merci du fond du coeur !
          </p>
          <Button name="Je soutiens" newPath="/" classes={"buttonContainer commonButton"} />
        </section>
        <section>
          <h2 className="title" tabIndex={0}>
            Faire remonter un <IonLabel color="secondary">bug</IonLabel>
          </h2>
          <div className="separator"></div>
          <p className="description">
            Votre aide est précieuse, faites nous remonter tout bug rencontré afin que nous puissons améliorer l’expérience de la communauté
            Smoothstair.{" "}
          </p>
          <Input classes="input" placeholder="Votre mail" />
          <InputArea placeholder="Détaillez le bug (problème / page)" />
          <Button name="Nous contacter" newPath="/" classes={"buttonContainer commonButton"} />
  </section>*/}
      </IonContent>
    </IonPage>
  );
};

export default UsPage;
