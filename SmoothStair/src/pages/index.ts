/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : index.ts
 * Description : Permet d'alléger les imports de pages dans le projet.
 ************************************************************************/

import HomePage from "./home/HomePage";
import NotFoundPage from "./NotFoundPage";
import ProfilPage from "./ProfilPage";
import Profil from "./profil/Profil";
import UsPage from "./UsPage";
import PlacesList from "./home/PlacesList";
import Questionnary from "./form/Questionnary";
import SituationsList from "./form/SituationsList";
import PlacesToAddPage from "./form/PlacesToAddPage";
import SituationToDescribePage from "./form/SituationsList";

export { HomePage, NotFoundPage, ProfilPage, Profil, UsPage, PlacesToAddPage, SituationToDescribePage, PlacesList, Questionnary, SituationsList };
