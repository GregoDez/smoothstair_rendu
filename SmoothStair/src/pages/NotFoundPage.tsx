/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : NotFoundPage.tsx
 * Description : Fallback en cas de 404
 ************************************************************************/

const NotFoundPage = () => {
  return (
    <div className="primaryFont">
      <h1>404</h1>
    </div>
  );
};
export default NotFoundPage;
