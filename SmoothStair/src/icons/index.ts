/************************************************************************
 * Auteur : Venezia Benjamin
 * Fichier : index.ts
 * Description : Permet d'alléger les imports dans le projet.
 ************************************************************************/

import wheelChair from "./accessibility/fauteuilSeul.svg";
import blind from "./accessibility/blind.svg";

import heart from "./social/heart.svg";
import like from "./social/like.svg";
import dislike from "./social/dislike.svg";
import cancel from "./social/cancel.svg";
import edit from "./social/edit.svg";

import awesome from "./accessibility/genial.svg";
import good from "./accessibility/bien.svg";
import satisfaisant from "./accessibility/satisfaisant.svg";
import bof from "./accessibility/bof.svg";
import angry from "./accessibility/pasBien.svg";
import nonRenseigner from "./accessibility/null.svg";

import banner from "../assets/img/banner.svg";
import us from "./us.svg";
import search from "./search.svg";
import resto from "./resto.svg";
import profil from "./profil.svg";
import home from "./home.svg";
import health from "./health.svg";
import gestion from "./gestion.svg";
import fun from "./fun.svg";
import commerce from "./commerce.svg";
import business from "./business.svg";
import backarrow from "./back-arrow.svg";
import addform from "./addform.svg";

export {
  wheelChair,
  blind,
  awesome,
  good,
  satisfaisant,
  bof,
  angry,
  banner,
  us,
  search,
  resto,
  profil,
  home,
  health,
  gestion,
  fun,
  commerce,
  business,
  backarrow,
  addform,
  nonRenseigner,
  heart,
  like,
  dislike,
  cancel,
  edit,
};
