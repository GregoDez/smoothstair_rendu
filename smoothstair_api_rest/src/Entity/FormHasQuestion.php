<?php

namespace App\Entity;

use App\Repository\FormHasQuestionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FormHasQuestionRepository::class)]
class FormHasQuestion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $idForm = null;

    #[ORM\Column]
    private ?int $idQuestion = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdForm(): ?int
    {
        return $this->idForm;
    }

    public function setIdForm(int $idForm): static
    {
        $this->idForm = $idForm;

        return $this;
    }

    public function getIdQuestion(): ?int
    {
        return $this->idQuestion;
    }

    public function setIdQuestion(int $idQuestion): static
    {
        $this->idQuestion = $idQuestion;

        return $this;
    }
}
