<?php

namespace App\Entity;

use App\Repository\FilledFormRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FilledFormRepository::class)]
class FilledForm
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $idForm = null;

    #[ORM\Column]
    private ?int $idPlace = null;

    #[ORM\Column]
    private ?int $idUser = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $date = null;

    #[ORM\Column]
    private ?int $nbLike = null;

    #[ORM\Column]
    private ?int $idNotation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdForm(): ?int
    {
        return $this->idForm;
    }

    public function setIdForm(int $idForm): static
    {
        $this->idForm = $idForm;

        return $this;
    }

    public function getIdPlace(): ?int
    {
        return $this->idPlace;
    }

    public function setIdPlace(int $idPlace): static
    {
        $this->idPlace = $idPlace;

        return $this;
    }

    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser): static
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getNbLike(): ?int
    {
        return $this->nbLike;
    }

    public function setNbLike(int $nbLike): static
    {
        $this->nbLike = $nbLike;

        return $this;
    }

    public function getIdNotation(): ?int
    {
        return $this->idNotation;
    }

    public function setIdNotation(int $idNotation): static
    {
        $this->idNotation = $idNotation;

        return $this;
    }
}
