<?php

namespace App\Entity;

use App\Repository\UserLikesFilledFormRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserLikesFilledFormRepository::class)]
class UserLikesFilledForm
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $idUser = null;

    #[ORM\Column]
    private ?int $idFilledForm = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser): static
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getIdFilledForm(): ?int
    {
        return $this->idFilledForm;
    }

    public function setIdFilledForm(int $idFilledForm): static
    {
        $this->idFilledForm = $idFilledForm;

        return $this;
    }
}
