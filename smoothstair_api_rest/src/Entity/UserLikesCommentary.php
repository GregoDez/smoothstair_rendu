<?php

namespace App\Entity;

use App\Repository\UserLikesCommentaryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserLikesCommentaryRepository::class)]
class UserLikesCommentary
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $idUser = null;

    #[ORM\Column]
    private ?int $idCommentary = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser): static
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getIdCommentary(): ?int
    {
        return $this->idCommentary;
    }

    public function setIdCommentary(int $idCommentary): static
    {
        $this->idCommentary = $idCommentary;

        return $this;
    }
}
