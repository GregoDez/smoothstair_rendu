<?php

namespace App\Entity;

use App\Repository\QuestionHasAnswerRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuestionHasAnswerRepository::class)]
class QuestionHasAnswer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $idQuestion = null;

    #[ORM\Column]
    private ?int $idAnswer = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdQuestion(): ?int
    {
        return $this->idQuestion;
    }

    public function setIdQuestion(int $idQuestion): static
    {
        $this->idQuestion = $idQuestion;

        return $this;
    }

    public function getIdAnswer(): ?int
    {
        return $this->idAnswer;
    }

    public function setIdAnswer(int $idAnswer): static
    {
        $this->idAnswer = $idAnswer;

        return $this;
    }
}
