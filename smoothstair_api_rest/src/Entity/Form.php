<?php

namespace App\Entity;

use App\Repository\FormRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FormRepository::class)]
class Form
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $idDisability = null;

    #[ORM\Column]
    private ?int $idSubCategory = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdDisability(): ?int
    {
        return $this->idDisability;
    }

    public function setIdDisability(int $idDisability): static
    {
        $this->idDisability = $idDisability;

        return $this;
    }

    public function getIdSubCategory(): ?int
    {
        return $this->idSubCategory;
    }

    public function setIdSubCategory(int $idSubCategory): static
    {
        $this->idSubCategory = $idSubCategory;

        return $this;
    }
}
