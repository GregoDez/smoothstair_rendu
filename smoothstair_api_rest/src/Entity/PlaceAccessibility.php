<?php

namespace App\Entity;

use App\Repository\PlaceAccessibilityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PlaceAccessibilityRepository::class)]
class PlaceAccessibility
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $idPlace = null;

    #[ORM\Column]
    private ?int $idDisability = null;

    #[ORM\Column]
    private ?int $idNotation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPlace(): ?int
    {
        return $this->idPlace;
    }

    public function setIdPlace(int $idPlace): static
    {
        $this->idPlace = $idPlace;

        return $this;
    }

    public function getIdDisability(): ?int
    {
        return $this->idDisability;
    }

    public function setIdDisability(int $idDisability): static
    {
        $this->idDisability = $idDisability;

        return $this;
    }

    public function getIdNotation(): ?int
    {
        return $this->idNotation;
    }

    public function setIdNotation(int $idNotation): static
    {
        $this->idNotation = $idNotation;

        return $this;
    }
}
