<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\QuestionHasAnswer;
use App\Entity\Question;
use App\Entity\Answer;

#[Route('/', name: 'api_')]
class QuestionHasAnswerController extends AbstractController
{
    #[Route('/questionHasAnswers', name: 'app_question_has_answer', methods:['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $questionHasAnswers = $doctrine->getRepository(QuestionHasAnswer::class)->findAll();

        $data = [];

        if (!$questionHasAnswers) {
            return $this->json('No answers for questions found ', 404);
        }        
   
        foreach ($questionHasAnswers as $questionHasAnswer) {
            $question = $doctrine->getRepository(Question::class)->find($questionHasAnswer->getIdQuestion());
            $answer = $doctrine->getRepository(Answer::class)->find($questionHasAnswer->getIdAnswer());

            $data[] = [
                'id' => $questionHasAnswer->getId(),
                'idQuestion' => $questionHasAnswer->getIdQuestion(),
                'question' => $question->getTitle(),
                'idAnswer' => $questionHasAnswer->getIdAnswer(),
                'answer' => $answer->getAnswer(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/questionHasAnswers/{id}', name: 'question_has_answers_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $questionHasAnswers = $doctrine->getRepository(QuestionHasAnswer::class)->findByIdQuestion($id);

        $data = [];

        if (!$questionHasAnswers) {
            return $this->json('No answers for this question found ', 404);
        }        
   
        foreach ($questionHasAnswers as $questionHasAnswer) {
            $question = $doctrine->getRepository(Question::class)->find($questionHasAnswer->getIdQuestion());
            $answer = $doctrine->getRepository(Answer::class)->find($questionHasAnswer->getIdAnswer());

            $data[] = [
                'id' => $questionHasAnswer->getId(),
                'idQuestion' => $questionHasAnswer->getIdQuestion(),
                'question' => $question->getTitle(),
                'idAnswer' => $questionHasAnswer->getIdAnswer(),
                'answer' => $answer->getAnswer(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/questionHasAnswers', name: 'question_has_answers_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        

        $questionHasAnswers = new QuestionHasAnswer();
        $questionHasAnswers->setIdQuestion($request->request->get('idQuestion'));
        $questionHasAnswers->setIdAnswer($request->request->get('idAnswer'));

        $entityManager->persist($questionHasAnswers);
        $entityManager->flush();
   
        $data =  [
            'id' => $questionHasAnswers->getId(),
            'idQuestion' => $questionHasAnswers->getIdQuestion(),
            'idAnswer' => $questionHasAnswers->getIdAnswer(),
        ];
           
        return $this->json($data);
    }

    #[Route('/questionHasAnswers/{id}', name: 'question_has_answers_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $questionHasAnswers = $entityManager->getRepository(QuestionHasAnswer::class)->find($id);
   
        if (!$questionHasAnswers) {
            return $this->json('No answer found for question id ' . $id, 404);
        }
   
        $entityManager->remove($questionHasAnswers);
        $entityManager->flush();
   
        return $this->json('Deleted a answer successfully for question with id ' . $id);
    }
}
