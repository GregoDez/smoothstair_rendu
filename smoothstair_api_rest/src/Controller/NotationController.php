<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Notation;
 
#[Route('/', name: 'api_')]
class NotationController extends AbstractController
{
    #[Route('/notations', name: 'notation_get', methods:['get'])]
    public function getAll(ManagerRegistry $doctrine): JsonResponse
    {
        $notations = $doctrine->getRepository(Notation::class)->findAll();

        $data = [];

        if (!$notations) {
            return $this->json('No notations found ', 404);
        }
   
        foreach ($notations as $notation) {
           $data[] = [
               'id' => $notation->getId(),
               'name' => $notation->getName(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/notations/{id}', name: 'notation_get_one', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $notation = $doctrine->getRepository(Notation::class)->find($id);
   
        if (!$notation) {
   
            return $this->json('No notation found for id ' . $id, 404);
        }
   
        $data =  [
            'id' => $notation->getId(),
            'name' => $notation->getName(),
        ];
           
        return $this->json($data);
    }
}
