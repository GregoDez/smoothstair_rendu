<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\FormHasQuestion;
use App\Entity\Form;
use App\Entity\Question;

#[Route('/', name: 'api_')]
class FormHasQuestionController extends AbstractController
{
    #[Route('/formHasQuestions', name: 'app_form_has_question', methods:['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $formHasQuestions = $doctrine->getRepository(formHasQuestion::class)->findAll();

        $data = [];

        if (!$formHasQuestions) {
            return $this->json('No question for form found ', 404);
        }        
   
        foreach ($formHasQuestions as $formHasQuestion) {
            $question = $doctrine->getRepository(Question::class)->find($formHasQuestion->getIdQuestion());
            $form = $doctrine->getRepository(Form::class)->find($formHasQuestion->getIdForm());

            $data[] = [
                'id' => $formHasQuestion->getId(),
                'idForm' => $formHasQuestion->getIdForm(),
                'idQuestion' => $formHasQuestion->getIdQuestion(),
                'question' => $question->getTitle(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/formHasQuestions/{id}', name: 'form_has_question_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $formHasQuestions = $doctrine->getRepository(FormHasQuestion::class)->findByidForm($id);

        $data = [];

        if (!$formHasQuestions) {
            return $this->json('No question for this form found ', 404);
        }        
   
        foreach ($formHasQuestions as $formHasQuestion) {
            $question = $doctrine->getRepository(Question::class)->find($formHasQuestion->getIdQuestion());
            $form = $doctrine->getRepository(Form::class)->find($formHasQuestion->getIdForm());

            $data[] = [
                'id' => $formHasQuestion->getId(),
                'idForm' => $formHasQuestion->getIdForm(),
                'idQuestion' => $formHasQuestion->getIdQuestion(),
                'question' => $question->getTitle(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/formHasQuestions', name: 'form_has_question_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        

        $formHasQuestion = new FormHasQuestion();
        $formHasQuestion->setIdForm($request->request->get('idForm'));
        $formHasQuestion->setIdQuestion($request->request->get('idQuestion'));

        $entityManager->persist($formHasQuestion);
        $entityManager->flush();
   
        $data =  [
            'id' => $formHasQuestion->getId(),
            'idForm' => $formHasQuestion->getIdForm(),
            'idQuestion' => $formHasQuestion->getIdQuestion(),
        ];
           
        return $this->json($data);
    }

    #[Route('/formHasQuestions/{id}', name: 'form_has_question_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $formHasQuestion = $entityManager->getRepository(FormHasQuestion::class)->find($id);
   
        if (!$formHasQuestion) {
            return $this->json('No question found for form id ' . $id, 404);
        }
   
        $entityManager->remove($formHasQuestion);
        $entityManager->flush();
   
        return $this->json('Deleted a question successfully for form with id ' . $id);
    }
}
