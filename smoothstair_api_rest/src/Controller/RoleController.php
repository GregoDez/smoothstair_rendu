<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Role;

#[Route('/', name: 'api_')]
class RoleController extends AbstractController
{
    #[Route('/roles', name: 'app_category')]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $roles = $doctrine->getRepository(Role::class)->findAll();

        $data = [];

        if (!$roles) {
            return $this->json('No roles found ', 404);
        }
   
        foreach ($roles as $role) {
           $data[] = [
               'id' => $role->getId(),
               'name' => $role->getName(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/roles/{id}', name: 'role_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $role = $doctrine->getRepository(Role::class)->find($id);
   
        if (!$role) {
   
            return $this->json('No role found for id ' . $id, 404);
        }
   
        $data =  [
            'id' => $role->getId(),
            'name' => $role->getName(),
        ];
           
        return $this->json($data);
    }
}
