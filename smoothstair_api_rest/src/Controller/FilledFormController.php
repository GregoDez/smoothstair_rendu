<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\FilledForm;
use App\Entity\SubCategory;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\Notation;
use App\Entity\Disability;

#[Route('/', name: 'api_')]
class FilledFormController extends AbstractController
{
    #[Route('/filledForms', name: 'app_filled_form', methods:['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $filledForms = $doctrine->getRepository(FilledForm::class)->findAll();

        $data = [];

        if (!$filledForms) {
            return $this->json('No filled forms for id found ', 404);
        }        
   
        foreach ($filledForms as $filledForm) {
            $notation = $doctrine->getRepository(Notation::class)->find($filledForm->getIdNotation());
            $user = $doctrine->getRepository(User::class)->find($filledForm->getIdUser());
            $role = $doctrine->getRepository(Role::class)->find($user->getIdRole());

            if($user->getIdDisability() == null) {
                $disabilityId = null;
                $disabilityType = null;
            } else {
                $disability = $doctrine->getRepository(Disability::class)->find($user->getIdDisability());
                $disabilityId = $user->getIdDisability();
                $disabilityType = $disability->getType();
            }    

            $data[] =  [
                'id' => $filledForm->getId(),
                'idForm' => $filledForm->getIdForm(),
                'idPlace' => $filledForm->getIdPlace(),
                'idUser' => $filledForm->getIdUser(),
                'pseudo' => $user->getPseudo(),
                'firstname' => $user->getFirstname(),
                'lastname' => $user->getName(),
                'idDisability' => $disabilityId,
                'disability' => $disabilityType,
                'idRole' => $user->getIdRole(),
                'role' => $role->getName(),
                'description' => $filledForm->getDescription(),
                'date' => $filledForm->getDate(),
                'nbLike' => $filledForm->getNbLike(),
                'idNotation' => $filledForm->getIdNotation(),
                'notation' => $notation->getName(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/filledForms/{id}', name: 'filled_form_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $filledForm = $doctrine->getRepository(FilledForm::class)->find($id);

        $data = [];

        if (!$filledForm) {
            return $this->json('No filled form for this id found ', 404);
        }     

        $notation = $doctrine->getRepository(Notation::class)->find($filledForm->getIdNotation());
        $user = $doctrine->getRepository(User::class)->find($filledForm->getIdUser());
        $role = $doctrine->getRepository(Role::class)->find($user->getIdRole());
        
        if($user->getIdDisability() == null) {
            $disabilityId = null;
            $disabilityType = null;
        } else {
            $disability = $doctrine->getRepository(Disability::class)->find($user->getIdDisability());
            $disabilityId = $user->getIdDisability();
            $disabilityType = $disability->getType();
        } 
        
        $data =  [
            'id' => $filledForm->getId(),
            'idForm' => $filledForm->getIdForm(),
            'idPlace' => $filledForm->getIdPlace(),
            'idUser' => $filledForm->getIdUser(),
            'pseudo' => $user->getPseudo(),
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getName(),
            'idDisability' => $disabilityId,
            'disability' => $disabilityType,
            'idRole' => $user->getIdRole(),
            'role' => $role->getName(),
            'description' => $filledForm->getDescription(),
            'date' => $filledForm->getDate(),
            'nbLike' => $filledForm->getNbLike(),
            'idNotation' => $filledForm->getIdNotation(),
            'notation' => $notation,
        ];
   
   
        return $this->json($data);
    }

    #[Route('/filledForms/byIdPlace/{id}', name: 'filled_form_get_by_id_place', methods:['get'] )]
    public function getByIdPlace(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $filledForms = $doctrine->getRepository(FilledForm::class)->findByIdPlace($id);

        $data = [];

        if (!$filledForms) {
            return $this->json('No filled forms for id found ', 404);
        }        
   
        foreach ($filledForms as $filledForm) {
            $notation = $doctrine->getRepository(Notation::class)->find($filledForm->getIdNotation());
            $user = $doctrine->getRepository(User::class)->find($filledForm->getIdUser());
            $role = $doctrine->getRepository(Role::class)->find($user->getIdRole());

            if($user->getIdDisability() == null) {
                $disabilityId = null;
                $disabilityType = null;
            } else {
                $disability = $doctrine->getRepository(Disability::class)->find($user->getIdDisability());
                $disabilityId = $user->getIdDisability();
                $disabilityType = $disability->getType();
            }    

            $data[] =  [
                'id' => $filledForm->getId(),
                'idForm' => $filledForm->getIdForm(),
                'idPlace' => $filledForm->getIdPlace(),
                'idUser' => $filledForm->getIdUser(),
                'pseudo' => $user->getPseudo(),
                'firstname' => $user->getFirstname(),
                'lastname' => $user->getName(),
                'idDisability' => $disabilityId,
                'disability' => $disabilityType,
                'idRole' => $user->getIdRole(),
                'role' => $role->getName(),
                'description' => $filledForm->getDescription(),
                'date' => $filledForm->getDate(),
                'nbLike' => $filledForm->getNbLike(),
                'idNotation' => $filledForm->getIdNotation(),
                'notation' => $notation->getName(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/filledForms', name: 'filled_form_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        
        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);

        $filledForm = new FilledForm();
        $filledForm->setIdForm($obj['idForm']);
        $filledForm->setIdPlace($obj['idPlace']);
        $filledForm->setIdUser($obj['idUser']);
        $filledForm->setDescription($obj['description']);
        $filledForm->setDate($obj['date']);
        $filledForm->setNbLike($obj['nbLike']);
        $filledForm->setIdNotation($obj['idNotation']);

        $entityManager->persist($filledForm);
        $entityManager->flush();
   
        $data =  [
            'id' => $filledForm->getId(),
            'idForm' => $filledForm->getIdForm(),
            'idPlace' => $filledForm->getIdPlace(),
            'idUser' => $filledForm->getIdUser(),
            'description' => $filledForm->getDescription(),
            'date' => $filledForm->getDate(),
            'nbLike' => $filledForm->getNbLike(),
            'idNotation' => $filledForm->getIdNotation(),
        ];
           
        return $this->json($data);
    }

    #[Route('/filledForms/{id}', name: 'filled_form_update', methods:['put', 'patch'] )]
    public function update(ManagerRegistry $doctrine, Request $request, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $filledForm = $entityManager->getRepository(FilledForm::class)->find($id);
   
        if (!$filledForm) {
            return $this->json('No filled form found for id' . $id, 404);
        }

        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);
   
        $filledForm->setNbLike($obj['nbLike']);
        $entityManager->flush();
   
        $data =  [
            'id' => $filledForm->getId(),
            'idForm' => $filledForm->getIdForm(),
            'idPlace' => $filledForm->getIdPlace(),
            'idUser' => $filledForm->getIdUser(),
            'description' => $filledForm->getDescription(),
            'date' => $filledForm->getDate(),
            'nbLike' => $filledForm->getNbLike(),
            'idNotation' => $filledForm->getIdNotation(),
        ];
           
        return $this->json($data);
    }

    #[Route('/filledForms/{id}', name: 'filled_form_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $filledForms = $entityManager->getRepository(FilledForm::class)->find($id);
   
        if (!$filledForms) {
            return $this->json('No filled for found for id ' . $id, 404);
        }
   
        $entityManager->remove($filledForms);
        $entityManager->flush();
   
        return $this->json('Deleted a form successfully with id ' . $id);
    }
}
