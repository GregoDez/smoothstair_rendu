<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Category;
 
#[Route('/', name: 'api_')]
class CategoryController extends AbstractController
{
    #[Route('/categories', name: 'category_get', methods:['get'])]
    public function getAll(ManagerRegistry $doctrine): JsonResponse
    {
        $categories = $doctrine->getRepository(Category::class)->findAll();

        $data = [];

        if (!$categories) {
            return $this->json('No categories found ', 404);
        }
   
        foreach ($categories as $category) {
           $data[] = [
               'id' => $category->getId(),
               'name' => $category->getName(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/categories/{id}', name: 'category_get_one', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $category = $doctrine->getRepository(Category::class)->find($id);
   
        if (!$category) {
   
            return $this->json('No category found for id ' . $id, 404);
        }
   
        $data =  [
            'id' => $category->getId(),
            'name' => $category->getName(),
        ];
           
        return $this->json($data);
    }
}
