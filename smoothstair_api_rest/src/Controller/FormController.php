<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Form;
use App\Entity\SubCategory;
use App\Entity\Disability;

#[Route('/', name: 'api_')]
class FormController extends AbstractController
{
    #[Route('/forms', name: 'form_answer', methods:['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $forms = $doctrine->getRepository(Form::class)->findAll();

        $data = [];

        if (!$forms) {
            return $this->json('No forms for questions found ', 404);
        }        
   
        foreach ($forms as $form) {
            $subcategory = $doctrine->getRepository(SubCategory::class)->find($form->getIdSubCategory());
            $disability = $doctrine->getRepository(Disability::class)->find($form->getIdDisability());

            $data[] = [
                'id' => $form->getId(),
                'idSubCategory' => $form->getIdSubCategory(),
                'subCategory' => $subcategory->getName(),
                'idDisability' => $form->getIdDisability(),
                'disability' => $disability->getType(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/forms/byIdSubCategory/{id}', name: 'form_by_id_place_answer', methods:['get'])]
    public function getByIdSubCategory(ManagerRegistry $doctrine,  int $id): JsonResponse
    {
        $forms = $doctrine->getRepository(Form::class)->findByIdSubCategory($id);

        $data = [];

        if (!$forms) {
            return $this->json('No forms for place found', 404);
        }        
   
        foreach ($forms as $form) {
            $subcategory = $doctrine->getRepository(SubCategory::class)->find($form->getIdSubCategory());
            $disability = $doctrine->getRepository(Disability::class)->find($form->getIdDisability());

            $data[] = [
                'id' => $form->getId(),
                'idSubCategory' => $form->getIdSubCategory(),
                'subCategory' => $subcategory->getName(),
                'idDisability' => $form->getIdDisability(),
                'disability' => $disability->getType(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/forms/{id}', name: 'form_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $form = $doctrine->getRepository(Form::class)->find($id);

        $data = [];

        if (!$form) {
            return $this->json('No form for this id found ', 404);
        }     

        $subcategory = $doctrine->getRepository(SubCategory::class)->find($form->getIdSubCategory());
        $disability = $doctrine->getRepository(Disability::class)->find($form->getIdDisability());
        
        $data = [
            'id' => $form->getId(),
            'idSubCategory' => $form->getIdSubCategory(),
            'subCategory' => $subcategory->getName(),
            'idDisability' => $form->getIdDisability(),
            'disability' => $disability->getType(),
        ];
   
   
        return $this->json($data);
    }

    #[Route('/forms', name: 'form_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        

        $form = new Form();
        $form->setIdSubCategory($request->request->get('idSubCategory'));
        $form->setIdDisability($request->request->get('idDisability'));

        $entityManager->persist($form);
        $entityManager->flush();
   
        $data =  [
            'id' => $form->getId(),
            'idSubCategory' => $form->getIdSubCategory(),
            'idDisability' => $form->getIdDisability(),
        ];
           
        return $this->json($data);
    }

    #[Route('/forms/{id}', name: 'form_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $form = $entityManager->getRepository(Form::class)->find($id);
   
        if (!$form) {
            return $this->json('No form found for id ' . $id, 404);
        }
   
        $entityManager->remove($form);
        $entityManager->flush();
   
        return $this->json('Deleted a form successfully with id ' . $id);
    }

}
