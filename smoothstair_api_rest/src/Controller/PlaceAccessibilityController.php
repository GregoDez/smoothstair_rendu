<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PlaceAccessibility;
use App\Entity\Notation;
use App\Entity\Disability;
use App\Entity\Place;

#[Route('/', name: 'api_')]
class PlaceAccessibilityController extends AbstractController
{
    

    #[Route('/placesAccessibilities', name: 'app_place_accessibility', methods: ['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $placeAccessibilities = $doctrine->getRepository(PlaceAccessibility::class)->findAll();

        $data = [];

        if (!$placeAccessibilities) {
            return $this->json('No accessibility found for id found', 404);
        }        
   
        foreach ($placeAccessibilities as $placeAccessibility) {
            $notation = $doctrine->getRepository(Notation::class)->find($placeAccessibility->getIdNotation());
            $disability = $doctrine->getRepository(Disability::class)->find($placeAccessibility->getIdDisability());
            
            $data[] =  [
                'id' => $placeAccessibility->getId(),
                'idDisability' => $placeAccessibility->getIdDisability(),
                'disability' => $disability->getType(),
                'idPlace' => $placeAccessibility->getIdPlace(),
                'idNotation' => $placeAccessibility->getIdNotation(),
                'notation' => $notation->getName(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/placesAccessibilities/{id}', name: 'place_accessibility_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $placeAccessibility = $doctrine->getRepository(PlaceAccessibility::class)->find($id);

        $data = [];

        if (!$placeAccessibility) {
            return $this->json('No accessibility for this id found ', 404);
        }     

        $notation = $doctrine->getRepository(Notation::class)->find($placeAccessibility->getIdNotation());
            $disability = $doctrine->getRepository(Disability::class)->find($placeAccessibility->getIdDisability());
        
        $data =  [
            'id' => $placeAccessibility->getId(),
            'idDisability' => $placeAccessibility->getIdDisability(),
            'disability' => $disability->getType(),
            'idPlace' => $placeAccessibility->getIdPlace(),
            'idNotation' => $placeAccessibility->getIdNotation(),
            'notation' => $notation->getName(),
        ];
   
   
        return $this->json($data);
    }

    #[Route('/placesAccessibilities/byPlaceId/{id}', name: 'place_accessibility_by_place_id_get', methods:['get'] )]
    public function getByIdPlace(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $placeAccessibilities = $doctrine->getRepository(PlaceAccessibility::class)->findByIdPlace($id);

        $data = [];

        if (!$placeAccessibilities) {
            return $this->json('No accessibility for this place found', 404);
        }     

        foreach ($placeAccessibilities as $placeAccessibility) {
            $notation = $doctrine->getRepository(Notation::class)->find($placeAccessibility->getIdNotation());
            $disability = $doctrine->getRepository(Disability::class)->find($placeAccessibility->getIdDisability());
            
            $data[] =  [
                'id' => $placeAccessibility->getId(),
                'idDisability' => $placeAccessibility->getIdDisability(),
                'disability' => $disability->getType(),
                'idPlace' => $placeAccessibility->getIdPlace(),
                'idNotation' => $placeAccessibility->getIdNotation(),
                'notation' => $notation->getName(),
            ];
        }
   
   
        return $this->json($data);
    }

    #[Route('/placesAccessibilities/{id}', name: 'placesAccessibilities_update', methods:['put', 'patch'] )]
    public function update(ManagerRegistry $doctrine, Request $request, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $placeAccessibility = $entityManager->getRepository(PlaceAccessibility::class)->find($id);
   
        if (!$placeAccessibility) {
            return $this->json('No commentary found for id' . $id, 404);
        }

        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);
   
        $placeAccessibility->setIdDisability($obj['idDisability']);
        $placeAccessibility->setIdPlace($obj['idPlace']);
        $placeAccessibility->setIdNotation($obj['idNotation']);
        $entityManager->flush();
   
        $data =  [
            'id' => $placeAccessibility->getId(),
            'idDisability' => $placeAccessibility->getIdDisability(),
            'idPlace' => $placeAccessibility->getIdPlace(),
            'idNotation' => $placeAccessibility->getIdNotation(),
        ];
           
        return $this->json($data);
    }

    #[Route('/placesAccessibilities', name: 'place_accessibiility_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);
   
        

        $placeAccessibility = new PlaceAccessibility();
        $placeAccessibility->setIdDisability($obj['idDisability']);
        $placeAccessibility->setIdPlace($obj['idPlace']);
        $placeAccessibility->setIdNotation($obj['idNotation']);

        $entityManager->persist($placeAccessibility);
        $entityManager->flush();
   
        $data =  [
            'id' => $placeAccessibility->getId(),
            'idDisability' => $placeAccessibility->getIdDisability(),
            'idPlace' => $placeAccessibility->getIdPlace(),
            'idNotation' => $placeAccessibility->getIdNotation(),
        ];
           
        return $this->json($data);
    }

    #[Route('/placesAccessibilities/{id}', name: 'place_accessibilities_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $placeAccessibility = $entityManager->getRepository(PlaceAccessibility::class)->find($id);
   
        if (!$placeAccessibility) {
            return $this->json('No accessibility for found for id ' . $id, 404);
        }
   
        $entityManager->remove($placeAccessibility);
        $entityManager->flush();
   
        return $this->json('Deleted a accessibility successfully with id ' . $id);
    }
}
