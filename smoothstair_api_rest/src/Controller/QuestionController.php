<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Question;

#[Route('/', name: 'api_')]
class QuestionController extends AbstractController
{
    #[Route('/questions', name: 'app_question', methods:['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $questions = $doctrine->getRepository(Question::class)->findAll();

        $data = [];

        if (!$questions) {
            return $this->json('No questions found ', 404);
        }
   
        foreach ($questions as $question) {
           $data[] = [
               'id' => $question->getId(),
               'title' => $question->getTitle(),
               'ponderation' => $question->getPonderation(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/questions/{id}', name: 'question_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $question = $doctrine->getRepository(Question::class)->find($id);
   
        if (!$question) {
   
            return $this->json('No question found for id ' . $id, 404);
        }
   
        $data =  [
            'id' => $question->getId(),
            'title' => $question->getTitle(),
            'ponderation' => $question->getPonderation(),
        ];
           
        return $this->json($data);
    }

    #[Route('/questions', name: 'question_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        

        $question = new Question();
        $question->setTitle($request->request->get('title'));
        $question->setPonderation($request->request->get('ponderation'));

        $entityManager->persist($question);
        $entityManager->flush();
   
        $data =  [
            'id' => $question->getId(),
            'title' => $question->getTitle(),
            'ponderation' => $question->getPonderation(),
        ];
           
        return $this->json($data);
    }

    #[Route('/questions/{id}', name: 'question_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $question = $entityManager->getRepository(Question::class)->find($id);
   
        if (!$question) {
            return $this->json('No question found for id ' . $id, 404);
        }
   
        $entityManager->remove($question);
        $entityManager->flush();
   
        return $this->json('Deleted a question successfully with id ' . $id);
    }
}
