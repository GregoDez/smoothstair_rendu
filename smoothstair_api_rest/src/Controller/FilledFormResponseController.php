<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\FilledFormResponse;
use App\Entity\Question;
use App\Entity\Answer;

#[Route('/', name: 'api_')]
class FilledFormResponseController extends AbstractController
{
    #[Route('/filledFormResponses', name: 'app_filled_form_response', methods:['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $filledFormResponses = $doctrine->getRepository(FilledFormResponse::class)->findAll();

        $data = [];

        if (!$filledFormResponses) {
            return $this->json('No responses filled form for id found ', 404);
        }        
   
        foreach ($filledFormResponses as $filledFormResponse) {
            $question = $doctrine->getRepository(Question::class)->find($filledFormResponse->getIdQuestion());
            $answer = $doctrine->getRepository(Answer::class)->find($filledFormResponse->getIdAnswer());

            $data[] =  [
                'id' => $filledFormResponse->getId(),
                'idFilledForm' => $filledFormResponse->getIdFilledForm(),
                'idQuestion' => $filledFormResponse->getIdQuestion(),
                'question' => $question->getTitle(),
                'idAnswer' => $filledFormResponse->getIdAnswer(),
                'answer' => $answer->getAnswer(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/filledFormResponses/{id}', name: 'filled_form_response_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $filledFormResponse = $doctrine->getRepository(FilledFormResponse::class)->find($id);

        $data = [];

        if (!$filledFormResponse) {
            return $this->json('No filled form response for this id found ', 404);
        }     
        
        $question = $doctrine->getRepository(Question::class)->find($filledFormResponse->getIdQuestion());
        $answer = $doctrine->getRepository(Answer::class)->find($filledFormResponse->getIdAnswer());

        $data =  [
            'id' => $filledFormResponse->getId(),
            'idFilledForm' => $filledFormResponse->getIdFilledForm(),
            'idQuestion' => $filledFormResponse->getIdQuestion(),
            'question' => $question->getTitle(),
            'idAnswer' => $filledFormResponse->getIdAnswer(),
            'answer' => $answer->getAnswer(),
        ];
   
   
        return $this->json($data);
    }

    #[Route('/filledFormResponses/byIdFilledForm/{id}', name: 'filled_form_response_get_by_id_filled_form', methods:['get'] )]
    public function getByIdFilledForm(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $filledFormResponses = $doctrine->getRepository(FilledFormResponse::class)->findByFilledForm($id);

        $data = [];

        if (!$filledFormResponses) {
            return $this->json('No filled form response for this id found ', 404);
        }     
        
        foreach ($filledFormResponses as $filledFormResponse) {
            $question = $doctrine->getRepository(Question::class)->find($filledFormResponse->getIdQuestion());
            $answer = $doctrine->getRepository(Answer::class)->find($filledFormResponse->getIdAnswer());

            $data[] =  [
                'id' => $filledFormResponse->getId(),
                'idFilledForm' => $filledFormResponse->getIdFilledForm(),
                'idQuestion' => $filledFormResponse->getIdQuestion(),
                'question' => $question->getTitle(),
                'idAnswer' => $filledFormResponse->getIdAnswer(),
                'answer' => $answer->getAnswer(),
            ];
        }
   
   
        return $this->json($data);
    }

    #[Route('/filledFormResponses', name: 'filled_form_response_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        
        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);

        $filledFormResponse = new FilledFormResponse();
        $filledFormResponse->setIdFilledForm($obj['idFilledForm']);
        $filledFormResponse->setIdQuestion($obj['idQuestion']);
        $filledFormResponse->setIdAnswer($obj['idAnswer']);

        $entityManager->persist($filledFormResponse);
        $entityManager->flush();
   
        $data =  [
            'id' => $filledFormResponse->getId(),
            'idFilledForm' => $filledFormResponse->getIdFilledForm(),
            'idQuestion' => $filledFormResponse->getIdQuestion(),
            'idAnswer' => $filledFormResponse->getIdAnswer(),
        ];
           
        return $this->json($data);
    }

    #[Route('/filledFormResponses/{id}', name: 'filled_form_response_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $filledFormResponse = $entityManager->getRepository(FilledFormResponse::class)->find($id);
   
        if (!$filledFormResponse) {
            return $this->json('No response for found for filled form id ' . $id, 404);
        }
   
        $entityManager->remove($filledFormResponse);
        $entityManager->flush();
   
        return $this->json('Deleted a response form successfully with id ' . $id);
    }
}
