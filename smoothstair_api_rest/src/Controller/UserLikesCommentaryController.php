<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\UserLikesCommentary;

#[Route('/', name: 'api_')]
class UserLikesCommentaryController extends AbstractController
{
    #[Route('/userLikesCommentaries', name: 'user_likes_commentaries_getAll', methods:['get'] )]
    public function getAll(ManagerRegistry $doctrine): JsonResponse
    {
        $userLikesCommentaries = $doctrine->getRepository(UserLikesCommentary::class)->findAll();
   
   
        $data = [];

        if (!$userLikesCommentaries) {
            return $this->json('No likes found', 404);
        }
   
        foreach ($userLikesCommentaries as $userLikesCommentary) {
           $data[] = [
               'id' => $userLikesCommentary->getId(),
               'idUser' => $userLikesCommentary->getIdUser(),
               'idCommentary' => $userLikesCommentary->getIdCommentary(),
           ];
        }
   
        return $this->json($data);
    }
    

    #[Route('/userLikesCommentaries/byIdCommentary/{id}', name: 'user_likes_commentaries_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $userLikesCommentaries = $doctrine->getRepository(UserLikesCommentary::class)->findByIdCommentary($id);
   
   
        $data = [];

        if (!$userLikesCommentaries) {
            return $this->json('No likes found', 404);
        }
   
        foreach ($userLikesCommentaries as $userLikesCommentary) {
           $data[] = [
               'id' => $userLikesCommentary->getId(),
               'idUser' => $userLikesCommentary->getIdUser(),
               'idCommentary' => $userLikesCommentary->getIdCommentary(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/userLikesCommentaries', name: 'user_likes_commentaries_create', methods:['post'])]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();

        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);
        
        $userLikesCommentary = new UserLikesCommentary();

        $userLikesCommentary->setIdUser($obj['idUser']);
        $userLikesCommentary->setIdCommentary($obj['idCommentary']);

        $entityManager->persist($userLikesCommentary);
        $entityManager->flush();
   
        $data = [
            'id' => $userLikesCommentary->getId(),
            'idUser' => $userLikesCommentary->getIdUser(),
            'idCommentary' => $userLikesCommentary->getIdCommentary(),
        ];
        
        return $this->json($data);
    }
    

    #[Route('/userLikesCommentaries/{id}', name: 'user_likes_commentaries_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $userLikesCommentary = $entityManager->getRepository(UserLikesCommentary::class)->find($id);
   
        if (!$userLikesCommentary) {
            return $this->json('No like found for id ' . $id, 404);
        }
   
        $entityManager->remove($userLikesCommentary);
        $entityManager->flush();
   
        return $this->json('Deleted a like successfully');
    }
}
