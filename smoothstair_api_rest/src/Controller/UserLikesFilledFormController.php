<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\UserLikesFilledForm;

#[Route('/', name: 'api_')]
class UserLikesFilledFormController extends AbstractController
{

    #[Route('/userLikesFilledForms', name: 'user_likes_filled_forms_getAll', methods:['get'] )]
    public function getAll(ManagerRegistry $doctrine): JsonResponse
    {
        $userLikesFilledForms = $doctrine->getRepository(UserLikesFilledForm::class)->findAll();
   
   
        $data = [];

        if (!$userLikesFilledForms) {
            return $this->json('No likes found', 404);
        }
   
        foreach ($userLikesFilledForms as $userLikesFilledForm) {
           $data[] = [
               'id' => $userLikesFilledForm->getId(),
               'idUser' => $userLikesFilledForm->getIdUser(),
               'idFilledForm' => $userLikesFilledForm->getIdFilledForm(),
           ];
        }
   
        return $this->json($data);
    }
    

    #[Route('/userLikesFilledForms/byIdFilledForm/{id}', name: 'user_likes_filled_forms_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $userLikesFilledForms = $doctrine->getRepository(UserLikesFilledForm::class)->findByIdFilledForm($id);
   
   
        $data = [];

        if (!$userLikesFilledForms) {
            return $this->json('No likes found', 404);
        }
   
        foreach ($userLikesFilledForms as $userLikesFilledForm) {
           $data[] = [
               'id' => $userLikesFilledForm->getId(),
               'idUser' => $userLikesFilledForm->getIdUser(),
               'idFilledForm' => $userLikesFilledForm->getIdFilledForm(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/userLikesFilledForms', name: 'user_likes_filled_forms_create', methods:['post'])]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();

        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);
        
        $userLikesFilledForm = new UserLikesFilledForm();

        $userLikesFilledForm->setIdUser($obj['idUser']);
        $userLikesFilledForm->setIdFilledForm($obj['idFilledForm']);

        $entityManager->persist($userLikesFilledForm);
        $entityManager->flush();
   
        $data = [
            'id' => $userLikesFilledForm->getId(),
            'idUser' => $userLikesFilledForm->getIdUser(),
            'idFilledForm' => $userLikesFilledForm->getIdFilledForm(),
        ];
        
        return $this->json($data);
    }
    

    #[Route('/userLikesFilledForms/{id}', name: 'user_likes_filled_forms_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $userLikesFilledForm = $entityManager->getRepository(UserLikesFilledForm::class)->find($id);
   
        if (!$userLikesFilledForm) {
            return $this->json('No like found for id ' . $id, 404);
        }
   
        $entityManager->remove($userLikesFilledForm);
        $entityManager->flush();
   
        return $this->json('Deleted a like successfully');
    }
}
