<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Answer;

#[Route('/', name: 'api_')]
class AnswerController extends AbstractController
{
    #[Route('/answers', name: 'app_answers', methods:['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $answers = $doctrine->getRepository(Answer::class)->findAll();

        $data = [];

        if (!$answers) {
            return $this->json('No answers found ', 404);
        }
   
        foreach ($answers as $answer) {
           $data[] = [
               'id' => $answer->getId(),
               'answer' => $answer->getAnswer(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/answers/{id}', name: 'answer_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $answer = $doctrine->getRepository(Answer::class)->find($id);
   
        if (!$answer) {
   
            return $this->json('No answer found for id ' . $id, 404);
        }
   
        $data =  [
            'id' => $answer->getId(),
            'answer' => $answer->getAnswer(),
        ];
           
        return $this->json($data);
    }

    #[Route('/answers', name: 'answer_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        

        $answer = new Answer();
        $answer->setAnswer($request->request->get('answer'));

        $entityManager->persist($answer);
        $entityManager->flush();
   
        $data =  [
            'id' => $answer->getId(),
            'answer' => $answer->getAnswer(),
        ];
           
        return $this->json($data);
    }

    #[Route('/answers/{id}', name: 'answer_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $answer = $entityManager->getRepository(Answer::class)->find($id);
   
        if (!$answer) {
            return $this->json('No answer found for id ' . $id, 404);
        }
   
        $entityManager->remove($answer);
        $entityManager->flush();
   
        return $this->json('Deleted a answer successfully with id ' . $id);
    }
}
