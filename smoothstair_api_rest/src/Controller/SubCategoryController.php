<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\SubCategory;
 
#[Route('/', name: 'api_')]
class SubCategoryController extends AbstractController
{
    #[Route('/subcategories', name: 'app_subcategory')]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $subCategories = $doctrine->getRepository(SubCategory::class)->findAll();

        $data = [];

        if (!$subCategories) {
            return $this->json('No sub categories found ', 404);
        }
   
        foreach ($subCategories as $subCategory) {
           $data[] = [
               'id' => $subCategory->getId(),
               'name' => $subCategory->getName(),
               'idCategory' => $subCategory->getIdCategory(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/subcategories/{id}', name: 'subcategory_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $subCategory = $doctrine->getRepository(SubCategory::class)->find($id);
   
        if (!$subCategory) {
   
            return $this->json('No sub category found for id ' . $id, 404);
        }
   
        $data =  [
            'id' => $subCategory->getId(),
            'name' => $subCategory->getName(),
        ];
           
        return $this->json($data);
    }
}
