<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Commentary;
use App\Entity\User;

#[Route('/', name: 'api_')]
class CommentaryController extends AbstractController
{
    #[Route('/commentaries/{commentaryText}/{date}/{nbLikes}/{idFilledForm}/{idUser}', name: 'commentary_create', methods:['get'])]
    public function new(ManagerRegistry $doctrine, Request $request, string $commentaryText, string $date, int $nbLikes, int $idFilledForm, int $idUser): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        
        $commentary = new Commentary();


        $commentary->setCommentary($commentaryText);
        $commentary->setDate($date);
        $commentary->setNbLikes($nbLikes);
        $commentary->setIdFilledForm($idFilledForm);
        $commentary->setIdUser($idUser);

        $entityManager->persist($commentary);
        $entityManager->flush();
   
        $data = [
            'id' => $commentary->getId(),
            'commentary' => $commentary->getCommentary(),
            'date' => $commentary->getDate(),
            'nbLikes' => $commentary->getNbLikes(),
            'idFilledForm' => $commentary->getIdFilledForm(),
            'idUser' => $commentary->getIdUser(),
        ];
           
        return $this->json($data);
    }

    #[Route('/commentaries', name: 'commentary_get_all', methods:['get'])]
    public function getAll(ManagerRegistry $doctrine): JsonResponse
    {
        $commentaries = $doctrine->getRepository(Commentary::class)->findAll();

        $data = [];

        if (!$commentaries) {
            return $this->json('No commentaries found ', 404);
        }        
   
        foreach ($commentaries as $commentary) {
            $user = $doctrine->getRepository(User::class)->find($commentary->getIdUser());

            $data[] = [
                'id' => $commentary->getId(),
                'commentary' => $commentary->getCommentary(),
                'date' => $commentary->getDate(),
                'nbLikes' => $commentary->getNbLikes(),
                'idFilledForm' => $commentary->getIdFilledForm(),
                'idUser' => $commentary->getIdUser(),
                'pseudo' => $user->getPseudo(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/commentaries', name: 'commentary_create', methods:['post'])]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();

        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);
        
        $commentary = new Commentary();

        $commentary->setCommentary($obj['commentaryText']);
        $commentary->setDate($obj['date']);
        $commentary->setNbLikes($obj['nbLikes']);
        $commentary->setIdFilledForm($obj['idFilledForm']);
        $commentary->setIdUser($obj['idUser']);

        $entityManager->persist($commentary);
        $entityManager->flush();
   
        $data = [
            'id' => $commentary->getId(),
            'commentary' => $commentary->getCommentary(),
            'date' => $commentary->getDate(),
            'nbLikes' => $commentary->getNbLikes(),
            'idFilledForm' => $commentary->getIdFilledForm(),
            'idUser' => $commentary->getIdUser(),
        ];
           
        return $this->json($data);
    }

    #[Route('/commentaries/byIdFilledForm/{id}', name: 'commentaries_by_id_filled_form', methods:['get'])]
    public function getByIdFilledForm(ManagerRegistry $doctrine,  int $id): JsonResponse
    {
        $commentaries = $doctrine->getRepository(Commentary::class)->findByIdFilledForm($id);

        $data = [];

        if (!$commentaries) {
            return $this->json('No commentaries found ', 404);
        }        
   
        foreach ($commentaries as $commentary) {
            $data[] = [
                'id' => $commentary->getId(),
                'commentary' => $commentary->getCommentary(),
                'date' => $commentary->getDate(),
                'nbLikes' => $commentary->getNbLikes(),
                'idFilledForm' => $commentary->getIdFilledForm(),
                'idUser' => $commentary->getIdUser(),
            ];
        }
   
        return $this->json($data);
    }


    #[Route('/commentaries/{id}', name: 'commentaries_update', methods:['put', 'patch'] )]
    public function update(ManagerRegistry $doctrine, Request $request, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $commentary = $entityManager->getRepository(Commentary::class)->find($id);
   
        if (!$commentary) {
            return $this->json('No commentary found for id' . $id, 404);
        }

        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);
        $commentaryText = $obj['commentary'];

        if($obj['nbLikes'] !== null) {
            $commentary->setNbLikes($obj['nbLikes']);
        }

        if($obj['commentary'] !== null) {
            $commentary->setCommentary($obj['commentary']);
        }
        
        $entityManager->flush();
   
        $data =  [
            'id' => $commentary->getId(),
            'commentary' => $commentary->getCommentary(),
            'date' => $commentary->getDate(),
            'nbLikes' => $commentary->getNbLikes(),
            'idFilledForm' => $commentary->getIdFilledForm(),
            'idUser' => $commentary->getIdUser(),
        ];
           
        return $this->json($data);
    }

    #[Route('/commentaries/{id}', name: 'commentary_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $commentary = $entityManager->getRepository(Commentary::class)->find($id);
   
        if (!$commentary) {
            return $this->json('No commentary found for id ' . $id, 404);
        }
   
        $entityManager->remove($commentary);
        $entityManager->flush();
   
        return $this->json('Deleted a commentary successfully with id ' . $id);
    }
}