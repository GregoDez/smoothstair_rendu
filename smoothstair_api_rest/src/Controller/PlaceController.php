<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Place;
use App\Entity\SubCategory;

#[Route('/', name: 'api_')]
class PlaceController extends AbstractController
{
    #[Route('/places', name: 'place_answer', methods:['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $places = $doctrine->getRepository(Place::class)->findAll();

        $data = [];

        if (!$places) {
            return $this->json('No places for questions found', 404);
        }        
   
        foreach ($places as $place) {
            $subcategory = $doctrine->getRepository(SubCategory::class)->find($place->getIdSubCategory());

            $data[] = [
                'id' => $place->getId(),
                'name' => $place->getName(),
                'address' => $place->getAddress(),
                'latitude' => $place->getLatitude(),
                'longitude' => $place->getLongitude(),
                'idSubCategory' => $place->getIdSubCategory(),
                'subCategory' => $subcategory->getName(),
            ];
        }
   
        return $this->json($data);
    }

    #[Route('/places/{id}', name: 'place_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $place = $doctrine->getRepository(Place::class)->find($id);

        $data = [];
        
        if (!$place) {
            return $this->json('No place for this id found ', 404);
        }     

        $subcategory = $doctrine->getRepository(SubCategory::class)->find($place->getIdSubCategory());
        
        $data = [
            'id' => $place->getId(),
            'name' => $place->getName(),
            'address' => $place->getAddress(),
            'latitude' => $place->getLatitude(),
            'longitude' => $place->getLongitude(),
            'idSubCategory' => $place->getIdSubCategory(),
            'subCategory' => $subcategory->getName(),
        ];
   
   
        return $this->json($data);
    }

    #[Route('/places/{name}/{address}', name: 'place_get_by_name_and_address', methods:['get'] )]
    public function getByNameAndAddress(ManagerRegistry $doctrine, string $name, string $address): JsonResponse
    {
        $place = $doctrine->getRepository(Place::class)->findByNameAndAddress($name, $address);

        $data = [];

        

        if (!$place) {
            return $this->json('No place found', 404);
        } else {
            $place = $place[0];
        }  

        $subcategory = $doctrine->getRepository(SubCategory::class)->find($place->getIdSubCategory());
        
        $data = [
            'id' => $place->getId(),
            'name' => $place->getName(),
            'address' => $place->getAddress(),
            'latitude' => $place->getLatitude(),
            'longitude' => $place->getLongitude(),
            'idSubCategory' => $place->getIdSubCategory(),
            'subCategory' => $subcategory->getName(),
        ];
   
   
        return $this->json($data);
    }

    #[Route('/places', name: 'place_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        
        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);

        $place = new Place();
        $place->setName($obj['name']);
        $place->setAddress($obj['address']);
        $place->setLatitude($obj['latitude']);
        $place->setLongitude($obj['longitude']);
        $place->setIdSubCategory($obj['idSubCategory']);

        $entityManager->persist($place);
        $entityManager->flush();
   
        $data = [
            'id' => $place->getId(),
            'name' => $place->getName(),
            'address' => $place->getAddress(),
            'latitude' => $place->getLatitude(),
            'longitude' => $place->getLongitude(),
            'idSubCategory' => $place->getIdSubCategory(),
        ];
           
        return $this->json($data);
    }

    #[Route('/places/{id}', name: 'place_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $place = $entityManager->getRepository(Place::class)->find($id);
   
        if (!$place) {
            return $this->json('No place found for id ' . $id, 404);
        }
   
        $entityManager->remove($place);
        $entityManager->flush();
   
        return $this->json('Deleted a place successfully with id ' . $id);
    }
}
