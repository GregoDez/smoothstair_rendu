<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Disability;

#[Route('/', name: 'api_')]
class DisabilityController extends AbstractController
{
    #[Route('/disabilities', name: 'app_disability', methods: ['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $disabilities = $doctrine->getRepository(Disability::class)->findAll();

        $data = [];

        if (!$disabilities) {
            return $this->json('No disabilities found ', 404);
        }
   
        foreach ($disabilities as $disability) {
           $data[] = [
               'id' => $disability->getId(),
               'type' => $disability->getType(),
           ];
        }
   
        return $this->json($data);
    }

    #[Route('/disabilities/{id}', name: 'disability_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $disability = $doctrine->getRepository(Disability::class)->find($id);
   
        if (!$disability) {
   
            return $this->json('No disability found for id ' . $id, 404);
        }
   
        $data =  [
            'id' => $disability->getId(),
            'type' => $disability->getType(),
        ];
           
        return $this->json($data);
    }

    #[Route('/disabilities', name: 'disabilities_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        

        $disability = new Disability();
        $disability->setType($request->request->get('type'));

        $entityManager->persist($disability);
        $entityManager->flush();
   
        $data =  [
            'id' => $disability->getId(),
            'type' => $disability->getType(),
        ];
           
        return $this->json($data);
    }
}
