<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\Disability;

#[Route('/', name: 'api_')]
class UserController extends AbstractController
{
    #[Route('/users/{id}', name: 'user_get', methods:['get'] )]
    public function get(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $user = $doctrine->getRepository(User::class)->find($id);
   
        if (!$user) {
            return $this->json('No user found for id ' . $id, 404);
        }

        $role = $doctrine->getRepository(Role::class)->find($user->getIdRole());
        $roleName = $role->getName();

        if($user->getIdDisability() == null) {
            $disabilityType = null;
        } else {
            $disability = $doctrine->getRepository(Disability::class)->find($user->getIdDisability());
            $disabilityType = $disability->getType();
        }        
        
   
        $data =  [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'firstname' => $user->getFirstname(),
            'pseudo' => $user->getPseudo(),
            'email' => $user->getEmail(),
            'idRole' => $user->getIdRole(),
            'role' => $roleName,
            'idDisability' => $user->getIdDisability(),
            'disability' => $disabilityType,
        ];
           
        return $this->json($data);
    }

    #[Route('/users/login', name: 'user_login', methods:['post'] )]
    public function login(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);

        $email = $obj['email'];
        //$pseudo = $request->request->get('pseudo');
        $password = $obj['password'];

        $user = $doctrine->getRepository(User::class)->findByEmail($email);
   
        if (!$user) {
            //$user = $doctrine->getRepository(User::class)->findByPseudo($pseudo);

            if(!$user) {
                return $this->json('No user found', 404);
            }
        }
            
        if($user[0]->getEmail() == $email /*|| $user->getPseudo() == $pseudo*/ && $user[0]->getPassword() == $password) {
            

            $data =  [
                'id' => $user[0]->getId(),
                'name' => $user[0]->getName(),
                'firstname' => $user[0]->getFirstname(),
                'pseudo' => $user[0]->getPseudo(),
                'email' => $user[0]->getEmail(),
                'idRole' => $user[0]->getIdRole(),
                'idDisability' => $user[0]->getIdDisability(),
            ];
               
            return $this->json($data, 200);

        } else {
            return $this->json('Incorrect credentials', 401);
        }
   
        
    }

    #[Route('/users/signup', name: 'user_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();

        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);

        $user = new User();
        $password = $obj['password'];
        $confirmationPassword = $obj['confirmationPassword'];
        $user->setPseudo($obj['pseudo']);
        $user->setEmail($obj['email']);
        $user->setIdRole($obj['idRole']);

        if($password === $confirmationPassword) {
            $user->setPassword($password);
        } else {
            return $this->json("Password don't match with the confirmation", 404);
        }
        

        if($obj['idDisability'] == null) {
            $idDisability = null;
        } else {
            $idDisability = $obj['idDisability'];
        }

        $user->setIdDisability($idDisability);

        $userExist = $doctrine->getRepository(User::class)->findByEmail($user->getEmail());

        if($userExist) {
            return $this->json('User already exist with this email ', 404);
        }
   
        $entityManager->persist($user);
        $entityManager->flush();
   
        $data =  [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'firstname' => $user->getFirstname(),
            'pseudo' => $user->getPseudo(),
            'email' => $user->getEmail(),
            'idRole' => $user->getIdRole(),
            'idDisability' => $user->getIdDisability(),
        ];
           
        return $this->json($data);
    }

    #[Route('/users/{email}', name: 'user_update', methods:['put', 'patch'] )]
    public function update(ManagerRegistry $doctrine, Request $request, string $email): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $user = $entityManager->getRepository(User::class)->findByEmail($email);
   
        if (!$user) {
            return $this->json('No user found for id ' . $id, 404);
        }

        $user = $user[0];

        $json = file_get_contents('php://input');
        $obj = json_decode($json, TRUE);
        $idRole = $obj['idRole'];
        $idDisability = (int)$obj['idDisability'];
   
        $user->setPseudo($obj['pseudo']);
        //$user->setEmail($request->request->get('email'));
        if($idRole !== null) {
            $user->setIdRole($idRole);
        }

        if($idDisability !== null) {
            $user->setIdDisability($idDisability);
        }
        
        $entityManager->flush();
   
        $data =  [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'firstname' => $user->getFirstname(),
            'pseudo' => $user->getPseudo(),
            'email' => $user->getEmail(),
            'idDisability' => $user->getIdDisability(),
            'idRole' => $user->getIdRole(),
        ];
           
        return $this->json($data);
    }

    #[Route('/users/{id}', name: 'user_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);
   
        if (!$user) {
            return $this->json('No user found for id ' . $id, 404);
        }
   
        $entityManager->remove($user);
        $entityManager->flush();
   
        return $this->json('Deleted a user successfully with id ' . $id);
    }
}
