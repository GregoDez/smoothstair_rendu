<?php

namespace App\Repository;

use App\Entity\QuestionHasAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<QuestionHasAnswer>
 *
 * @method QuestionHasAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionHasAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionHasAnswer[]    findAll()
 * @method QuestionHasAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionHasAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuestionHasAnswer::class);
    }

    /**
     * @return QuestionHasAnswer[] Returns an array of QuestionHasAnswer objects
     */
    public function findByIdQuestion($value): array
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.idQuestion = :idQuestion')
            ->setParameter('idQuestion', $value)
            ->orderBy('q.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?QuestionHasAnswer
//    {
//        return $this->createQueryBuilder('q')
//            ->andWhere('q.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
