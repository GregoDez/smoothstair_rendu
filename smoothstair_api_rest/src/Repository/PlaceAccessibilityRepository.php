<?php

namespace App\Repository;

use App\Entity\PlaceAccessibility;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PlaceAccessibility>
 *
 * @method PlaceAccessibility|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceAccessibility|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceAccessibility[]    findAll()
 * @method PlaceAccessibility[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceAccessibilityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaceAccessibility::class);
    }

    /**
     * @return PlaceAccessibility[] Returns an array of PlaceAccessibility objects
     */
    public function findByIdPlace($value): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.idPlace = :idPlace')
            ->setParameter('idPlace', $value)
            ->orderBy('p.idDisability', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?PlaceAccessibility
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
