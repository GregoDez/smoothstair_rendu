<?php

namespace App\Repository;

use App\Entity\FormHasQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FormHasQuestion>
 *
 * @method FormHasQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormHasQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormHasQuestion[]    findAll()
 * @method FormHasQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormHasQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormHasQuestion::class);
    }

    /**
     * @return QuestionHasAnswer[] Returns an array of QuestionHasAnswer objects
     */
    public function findByIdForm($value): array
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.idForm = :idForm')
            ->setParameter('idForm', $value)
            ->orderBy('q.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?FormHasQuestion
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
