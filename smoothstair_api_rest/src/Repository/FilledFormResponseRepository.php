<?php

namespace App\Repository;

use App\Entity\FilledFormResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FilledFormResponse>
 *
 * @method FilledFormResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilledFormResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilledFormResponse[]    findAll()
 * @method FilledFormResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilledFormResponseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilledFormResponse::class);
    }

    /**
     * @return FilledFormResponse[] Returns an array of QuestionHasAnswer objects
     */
    public function findByFilledForm($value): array
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.idFilledForm = :idFilledForm')
            ->setParameter('idFilledForm', $value)
            ->orderBy('q.idQuestion', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?FilledFormResponse
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
