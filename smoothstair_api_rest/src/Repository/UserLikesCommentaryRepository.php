<?php

namespace App\Repository;

use App\Entity\UserLikesCommentary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserLikesCommentary>
 *
 * @method UserLikesCommentary|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLikesCommentary|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLikesCommentary[]    findAll()
 * @method UserLikesCommentary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLikesCommentaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLikesCommentary::class);
    }

    /**
     * @return UserLikesCommentary[] Returns an array of Commentary objects
     */
    public function findByIdFilledForm($value): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.idCommentary = :idCommentary')
            ->setParameter('idCommentary', $value)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    /**
//     * @return UserLikesCommentary[] Returns an array of UserLikesCommentary objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UserLikesCommentary
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
