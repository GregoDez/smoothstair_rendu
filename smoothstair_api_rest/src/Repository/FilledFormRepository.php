<?php

namespace App\Repository;

use App\Entity\FilledForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FilledForm>
 *
 * @method FilledForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilledForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilledForm[]    findAll()
 * @method FilledForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilledFormRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilledForm::class);
    }

    public function findAll()
    {
        return $this->findBy(array(), array('nbLike' => 'DESC'));
    }

    /**
     * @return FilledForm[] Returns an array of FilledForm objects
     */
    public function findByIdPlace($value): array
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.idPlace = :idPlace')
            ->setParameter('idPlace', $value)
            ->orderBy('f.date', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?FilledForm
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
