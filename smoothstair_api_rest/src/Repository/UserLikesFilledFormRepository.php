<?php

namespace App\Repository;

use App\Entity\UserLikesFilledForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserLikesFilledForm>
 *
 * @method UserLikesFilledForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLikesFilledForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLikesFilledForm[]    findAll()
 * @method UserLikesFilledForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLikesFilledFormRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLikesFilledForm::class);
    }

    /**
     * @return UserLikesFilledForm[] Returns an array of Commentary objects
     */
    public function findByIdFilledForm($value): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.idFilledForm = :idFilledForm')
            ->setParameter('idFilledForm', $value)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    /**
//     * @return UserLikesFilledForm[] Returns an array of UserLikesFilledForm objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UserLikesFilledForm
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
