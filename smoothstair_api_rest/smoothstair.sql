-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: smoothstair.mysql.db
-- Generation Time: Aug 13, 2023 at 11:03 PM
-- Server version: 5.7.42-log
-- PHP Version: 8.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smoothstair`
--
CREATE DATABASE IF NOT EXISTS `smoothstair` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `smoothstair`;

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `answer`) VALUES
(1, 'Oui'),
(2, 'Non'),
(3, 'Je ne sais pas');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Commerces'),
(2, 'Gestion'),
(3, 'Santé'),
(4, 'Services'),
(5, 'Boire et manger'),
(6, 'Loisirs');

-- --------------------------------------------------------

--
-- Table structure for table `commentary`
--

CREATE TABLE IF NOT EXISTS `commentary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentary` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_likes` int(11) NOT NULL,
  `id_filled_form` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commentary`
--

INSERT INTO `commentary` (`id`, `commentary`, `date`, `nb_likes`, `id_filled_form`, `id_user`) VALUES
(60, '', '07/08/2023', 0, 0, 1),
(74, 'blabla', '2023-8-13', 0, 87, 1),
(76, 'hello', '2023-8-13', 0, 2, 1),
(77, 'ooooook', '2023-8-13', 1, 2, 1),
(78, 'dwadwadwa', '2023-8-13', 0, 2, 1),
(79, 'grdgdrgrd', '2023-8-13', 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `disability`
--

CREATE TABLE IF NOT EXISTS `disability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `disability`
--

INSERT INTO `disability` (`id`, `type`) VALUES
(1, 'Chaise roulante'),
(2, 'Aveugle');

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20230803134101', '2023-08-03 15:41:51', 44),
('DoctrineMigrations\\Version20230803134952', '2023-08-03 15:49:59', 100),
('DoctrineMigrations\\Version20230803141228', '2023-08-03 16:12:33', 15),
('DoctrineMigrations\\Version20230803171036', '2023-08-03 19:10:40', 20),
('DoctrineMigrations\\Version20230808143758', '2023-08-08 16:38:05', 34),
('DoctrineMigrations\\Version20230809120420', '2023-08-09 14:04:26', 22),
('DoctrineMigrations\\Version20230811201653', '2023-08-11 22:16:57', 59);

-- --------------------------------------------------------

--
-- Table structure for table `filled_form`
--

CREATE TABLE IF NOT EXISTS `filled_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `id_place` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_like` int(11) NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_notation` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `filled_form`
--

INSERT INTO `filled_form` (`id`, `id_form`, `id_place`, `id_user`, `description`, `nb_like`, `date`, `id_notation`) VALUES
(2, 1, 1, 2, 'Impossible d\'aller aux toilettes...', 1, '05/08/2023', 5),
(3, 1, 1, 1, 'Innaccessible', 1, '08/08/2023', 5),
(76, 1, 26, 1, 'fhfthtfhtf', 0, '12/08/2023', 2),
(78, 1, 26, 1, 'drbvd', 0, '12/08/2023', 5),
(79, 1, 26, 1, 'sfesfes', 0, '12/08/2023', 1),
(80, 1, 26, 1, 'cest le dernier test', 0, '12/08/2023', 5),
(81, 1, 28, 1, 'wdawdawa', 0, '12/08/2023', 5),
(83, 2, 29, 1, 'rdgdrrdgdr', 0, '13/08/2023', 5),
(84, 1, 2, 1, 'rgtrf', 0, '13/08/2023', 2),
(86, 2, 26, 1, 'htfhtfh', 0, '13/08/2023', 2),
(88, 1, 26, 1, 'je sais pas quoi dire', 0, '13/08/2023', 2);

-- --------------------------------------------------------

--
-- Table structure for table `filled_form_response`
--

CREATE TABLE IF NOT EXISTS `filled_form_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_filled_form` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `id_answer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `filled_form_response`
--

INSERT INTO `filled_form_response` (`id`, `id_filled_form`, `id_question`, `id_answer`) VALUES
(1, 3, 1, 1),
(2, 3, 2, 1),
(3, 3, 3, 1),
(4, 2, 1, 1),
(5, 2, 2, 1),
(126, 2, 3, 1),
(211, 76, 3, 2),
(212, 76, 1, 1),
(213, 76, 2, 1),
(217, 78, 3, 2),
(218, 78, 1, 2),
(219, 78, 2, 2),
(220, 79, 1, 1),
(221, 79, 2, 1),
(222, 79, 3, 1),
(223, 80, 2, 2),
(224, 80, 1, 2),
(225, 80, 3, 2),
(226, 81, 2, 2),
(227, 81, 3, 1),
(228, 81, 1, 2),
(229, 82, 3, 2),
(230, 82, 4, 1),
(231, 83, 3, 2),
(232, 83, 4, 2),
(233, 84, 1, 1),
(234, 84, 3, 2),
(235, 84, 2, 1),
(236, 85, 4, 2),
(237, 85, 3, 1),
(238, 86, 3, 2),
(239, 86, 4, 1),
(240, 87, 3, 1),
(241, 87, 4, 2),
(242, 88, 1, 1),
(243, 88, 2, 1),
(244, 88, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_disability` int(11) NOT NULL,
  `id_sub_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `id_disability`, `id_sub_category`) VALUES
(1, 1, 19),
(2, 2, 19);

-- --------------------------------------------------------

--
-- Table structure for table `form_has_question`
--

CREATE TABLE IF NOT EXISTS `form_has_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `form_has_question`
--

INSERT INTO `form_has_question` (`id`, `id_form`, `id_question`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 3),
(5, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `notation`
--

CREATE TABLE IF NOT EXISTS `notation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notation`
--

INSERT INTO `notation` (`id`, `name`) VALUES
(1, 'Genial'),
(2, 'Bien'),
(3, 'Moyen'),
(4, 'Mauvais'),
(5, 'Inadmissible'),
(6, 'Non Renseigné');

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE IF NOT EXISTS `place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_sub_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`id`, `name`, `address`, `latitude`, `longitude`, `id_sub_category`) VALUES
(1, 'Brasserie du Lignon', 'Place du lignon 10, 1219 Le Lignon', '46.203768873792505', '6.095897876721996', 19),
(2, 'Da Toni', 'Place du Lignon 4, 1219 Vernier', '46.20446157592436', '6.097273971163234', 19),
(26, 'McDonald’s Restaurant', 'Rte du Bois-des-Frères 40, 1219 Aïre, Suisse', '46.2059684', '6.098786', 19),
(28, 'McDonald’s Restaurant', 'Av. Louis-Casaï 27, 1220 Vernier, Suisse', '46.2189309', '6.1138335', 19);

-- --------------------------------------------------------

--
-- Table structure for table `place_accessibility`
--

CREATE TABLE IF NOT EXISTS `place_accessibility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_place` int(11) NOT NULL,
  `id_disability` int(11) NOT NULL,
  `id_notation` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `place_accessibility`
--

INSERT INTO `place_accessibility` (`id`, `id_place`, `id_disability`, `id_notation`) VALUES
(1, 1, 1, 5),
(3, 2, 1, 2),
(4, 2, 2, 5),
(5, 1, 2, 5),
(8, 3, 1, 6),
(9, 3, 2, 6),
(56, 26, 2, 2),
(57, 26, 1, 2),
(62, 28, 2, 6),
(63, 28, 1, 5),
(64, 29, 2, 5),
(65, 29, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ponderation` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `title`, `ponderation`) VALUES
(1, 'Le lieu est-il atteignable en fauteuil roulant ?', 4),
(2, 'La porte d\'entrée est-elle suffisamment large ?', 4),
(3, 'La porte d\'entrée est-elle automatique ?', 1),
(4, 'Les chiens sont-ils acceptés ?', 4);

-- --------------------------------------------------------

--
-- Table structure for table `question_has_answer`
--

CREATE TABLE IF NOT EXISTS `question_has_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_question` int(11) NOT NULL,
  `id_answer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_has_answer`
--

INSERT INTO `question_has_answer` (`id`, `id_question`, `id_answer`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 1, 2),
(5, 2, 2),
(6, 3, 2),
(9, 4, 1),
(10, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Fondateur'),
(3, 'Pro'),
(4, 'Utilisateur');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `name`, `id_category`) VALUES
(1, 'Commerces à l\'étalage', 1),
(2, 'Grands magasins', 1),
(3, 'Centre comerciaux', 1),
(4, 'Electronique', 1),
(5, 'Librairies', 1),
(6, 'Papèterie', 1),
(7, 'Mobilier', 1),
(8, 'Commerces ambulants', 1),
(9, 'Banques', 2),
(10, 'Postes', 2),
(11, 'Assurances', 2),
(12, 'Aide sociale', 2),
(13, 'Appartement', 2),
(14, 'Hôpital', 3),
(15, 'Permanence', 3),
(16, 'Cabinet', 3),
(17, 'Pharmacie', 3),
(18, 'Bars', 5),
(19, 'Restaurants', 5),
(20, 'Fast-foods', 5),
(21, 'Terasses', 5),
(22, 'Festivals', 6),
(23, 'Salles de concert', 6),
(24, 'Stades', 6),
(25, 'Cinémas', 6),
(26, 'Théâtres', 6),
(27, 'Musées', 6),
(28, 'Bibliothèque', 6),
(29, 'Discothèque', 6);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_role` int(11) NOT NULL,
  `id_disability` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `firstname`, `pseudo`, `id_role`, `id_disability`, `email`, `password`) VALUES
(1, 'Mendez', 'Grégory', 'Greeeeeg', 2, 0, 'gregory@gmail.com', '79e2475f81a6317276bf7cbb3958b20d289b78df'),
(2, 'Mendez', 'Nadja', 'NadjaMendez', 4, 1, 'nadjamendez@bluewin.ch', '79e2475f81a6317276bf7cbb3958b20d289b78df'),
(7, 'Mendez', 'Noémieee', 'NoeMendez', 4, 2, 'noee@gmail.com', '79e2475f81a6317276bf7cbb3958b20d289b78df'),
(8, 'Mendez', 'Noémieee', 'NoeMendez', 4, 2, 'noeee@gmail.com', '79e2475f81a6317276bf7cbb3958b20d289b78df'),
(12, NULL, NULL, 'reg', 4, NULL, 'gregoryy@gmail.com', '79e2475f81a6317276bf7cbb3958b20d289b78df'),
(13, NULL, NULL, 'greg', 4, NULL, 'greg@gmail.coom', '79e2475f81a6317276bf7cbb3958b20d289b78df'),
(14, NULL, NULL, '', 4, NULL, '', 'da39a3ee5e6b4b0d3255bfef95601890afd80709'),
(15, NULL, NULL, '', 4, NULL, 'grdgrd', 'da39a3ee5e6b4b0d3255bfef95601890afd80709'),
(16, NULL, NULL, '', 4, NULL, 'GREG', 'da39a3ee5e6b4b0d3255bfef95601890afd80709');

-- --------------------------------------------------------

--
-- Table structure for table `user_likes_commentary`
--

CREATE TABLE IF NOT EXISTS `user_likes_commentary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_commentary` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_likes_commentary`
--

INSERT INTO `user_likes_commentary` (`id`, `id_user`, `id_commentary`) VALUES
(14, 12, 62),
(23, 1, 65);

-- --------------------------------------------------------

--
-- Table structure for table `user_likes_filled_form`
--

CREATE TABLE IF NOT EXISTS `user_likes_filled_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_filled_form` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_likes_filled_form`
--

INSERT INTO `user_likes_filled_form` (`id`, `id_user`, `id_filled_form`) VALUES
(160, 12, 2),
(162, 12, 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
